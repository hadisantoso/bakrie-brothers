# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


(1...10).each do |n|

  name = ["fry", "leela", "ship"].shuffle.first

  Crew.create(
    {
      age: rand(20) + 7,
      name: name + "_#{rand(1000)}",
      avatar: name + ".jpg",
      origin: ["Singapore", "Indonesia", "USA"].shuffle.first,
      species: ["Human", "Martian", "Venusian"].shuffle.first,
      title: ["Mr", "Ms", "Miss"].shuffle.first
    }
  )

end

