require 'carrierwave/mongoid'
require 'carrierwave/processing/mini_magick'

if Rails.env.production? || Rails.env.staging?
  CarrierWave.configure do |config|
    config.storage          = :fog
    config.fog_public       = true
    config.fog_directory    = "bakrie-brothers"
    config.fog_attributes   = {'Cache-Control' => 'public, max-age=315576000'}
    config.asset_host       = 'https://s3-ap-southeast-1.amazonaws.com/bakrie-brothers'
    config.fog_credentials  = {
      :provider               => 'AWS',
      :region                 => 'ap-southeast-1',
      :aws_access_key_id      => ENV["AWS_ACCESS_KEY_ID"],
      :aws_secret_access_key  => ENV["AWS_SECRET_ACCESS_KEY"]
    }
  end
else
  CarrierWave.configure do |config|
    config.storage          = :fog
    config.fog_public       = true
    config.fog_directory    = "bakriebrotherstest"
    config.fog_attributes   = {'Cache-Control' => 'public, max-age=315576000'}
    config.asset_host       = 'https://s3-ap-southeast-1.amazonaws.com/bakriebrotherstest'
    config.fog_credentials  = {
      :provider               => 'AWS',
      :region                 => 'ap-southeast-1',
      :aws_access_key_id      => Rails.application.secrets.aws_access_key_id,
      :aws_secret_access_key  => Rails.application.secrets.aws_secret_access_key
    }
  end
end
