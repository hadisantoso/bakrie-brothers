Rails.application.routes.draw do

  match '*path', :controller => 'application', :action => 'handle_options_request', :constraints => {:method => 'OPTIONS'}, via: [:options]

  namespace :admin do
    resources :crew do
      post 'upload_image', action: :upload_image
      post 'upload_document', action: :upload_document
    end
    resources :user do
      post 'upload_image', action: :upload_image
      post 'upload_document', action: :upload_document
    end
    resources :post do
      post 'upload_image', action: :upload_image
      post 'upload_image2', action: :upload_image2
      post 'upload_image3', action: :upload_image3
      post 'upload_image4', action: :upload_image4
      post 'upload_header_image', action: :upload_header_image
      post 'upload_document', action: :upload_document
      post 'upload_document2', action: :upload_document2
    end
    resources :auth do
      collection do 
        post 'authenticate', action: :authenticate
      end
    end

    resources :auditor

    resources :backup
  end

  namespace :end_user do
    resources :post
    resources :home
  end

  resources :pdf do
    collection do
      get 'corporate_history', action: :corporate_history
    end
  end

  root :to => "application#index"

  match "*path", :controller => 'application', :action => 'not_found', via: :all
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
