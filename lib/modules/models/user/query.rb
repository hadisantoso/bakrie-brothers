module Modules::Models::User::Query
  def self.included(base)
      base.extend(ClassMethods)
  end

  module ClassMethods      
    def find_by_token (token)
      User.where(:token => token).first
    end

    def find_by_email_token (token)
      User.where(:email_token => token).first
    end

    def find_all (user_id)
      user_id = user_id.to_s
      if user_id.to_s.match(/^[0-9a-fA-F]{24}$/)
        u= User.where(:_id=>user_id).first
      else
        u= User.find_by_nickname(user_id)
      end
      u
    end

    def find_by_nickname(nickname); User.where(:nickname=>nickname.downcase).first; end
    
    def find_by_email(email); User.where(:email=>email).first; end
  end
end