module Modules::Models::User::Security
  def self.included(base)
      base.extend(ClassMethods)
  end

  def get_email_token
    if self.email_token.nil?
      self.email_token = self.generate_email_token
      self.save
    end
    
    return self.email_token
  end
  
  def send_password_reset
    generate_reset_token(:password_reset_token)
    self.password_reset_sent_at = Time.zone.now
    save!
    UserMailer.delay.password_reset(self.id)
  end

  def generate_reset_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.where(column => self[column]).first
  end

  def generate_token
    self.token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.where(:token => random_token).exists?
    end
  end

  def generate_email_token
    self.email_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.where(:email_token => random_token).exists?
    end
  end

  def setup_password
    puts "setup password"
    if self.password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(self.password, self.password_salt)
    end
  end

  module ClassMethods
    # authenticate
    def authenticate(id, password)
      user = User.find_all(id)
          
      if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
        user.save!
        puts "authentication successful"
        user
      else
        puts "authentication failed"
        nil
      end
    end

    def generate_random_password
      return (0...12).map { ('a'..'z').to_a[rand(26)] }.join
    end
  end
end