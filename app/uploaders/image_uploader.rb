class ImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::MiniMagick

  def store_dir
    "uploads/images/#{mounted_as}/#{model.id}"
  end

  # # # Create different versions of your uploaded files:
  version :low_resolution do
    process :resize_to_limit => [250, nil]
  end

  version :standard_resolution do
    process :resize_to_limit => [612, nil]
  end

  # # Create different versions of your uploaded files:
  version :thumbnail do
    process :resize_to_limit => [100, 100]
  end

end
