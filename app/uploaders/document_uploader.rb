class DocumentUploader < CarrierWave::Uploader::Base

  def store_dir
    "uploads/documents/#{mounted_as}/#{model.id}"
  end

end
