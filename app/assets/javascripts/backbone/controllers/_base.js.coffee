@BBApp.module "Controllers", (Controllers, App, Backbone, Marionette, $, _) ->
	
	class Controllers.Base extends Marionette.Controller
		
		constructor: (options = {}) ->
			console.log "Controller is constructed"
			@region = options.region or App.request "default:region"
			console.log "Controller's region is: #{@region}"
			super options
			@_instance_id = _.uniqueId("controller")
			App.execute "register:instance", @, @_instance_id
		
		close: (args...) ->
			delete @region
			delete @options
			super args
			App.execute "unregister:instance", @, @_instance_id
		
		show: (view) ->
			console.log view
			console.log @
			@listenTo view, "close", @close
			@region.show view