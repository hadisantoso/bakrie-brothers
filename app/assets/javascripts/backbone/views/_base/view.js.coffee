@BBApp.module "Views", (Views, App, Backbone, Marionette, $, _) ->
	
	_remove = Marionette.View::remove
	
	_.extend Marionette.View::,
	
		addOpacityWrapper: (init = true) ->
			@$el.toggleWrapper
				className: "opacity"
			, init
	
		setInstancePropertiesFor: (args...) ->
			for key, val of _.pick(@options, args...)
				@[key] = val
	
		remove: (args...) ->
			console.log "removing", @
			if @model?.isDestroyed?()
				
				wrapper = @$el.toggleWrapper
					className: "opacity"
					backgroundColor: "red"
				
				wrapper.fadeOut 400, ->
					$(@).remove()
				
				@$el.fadeOut 400, =>
					_remove.apply @, args
			else
				_remove.apply @, args
	
		templateHelpers: ->

			currentFragment: ->
				return Backbone.history.getFragment()

			language: ->
				App.language

			resolvedTitle: ->
				if @language() == "id" 
					return @title.capitalize() 
				else 
					return @title2.capitalize()

			resolvedHeaderLink: ->
				if @language() == "id" 
					return @header_link_url 
				else 
					return @header2_link_url

			resolvedVideo: ->
				if @language() == "id" 
					return @video_url 
				else 
					return @video2_url

			resolvedContent: ->
				if @language() == "id" 
					return @custom_markdown_html 
				else 
					return @custom_markdown_html2

			resolvedSummaryContent: ->
				if @language() == "id" 
					return @summary_markdown_html 
				else 
					return @summary_markdown_html2

			resolvedDocumentUrl: ->
				if @language() == "id" 
					return @document.document_url
				else 
					return @document2.document2_url

			emptyDocumentUrl: ->
				if @language() == "id" 
					return @document.document_url == ""
				else 
					return @document2.document2_url == ""

			translate: (name) ->
				App.Languages.translate(name)
			
			loggedInNickname: ->
				return App.loggedInNickname

			loggedInDepartment: ->
				return App.loggedInDepartment

			loggedInPostAccess: ->
				return App.loggedInPostAccess

			linkTo: (name, url, options = {}) ->
				_.defaults options,
					external: false
				
				url = "#" + url unless options.external
				
				"<a href='#{url}'>#{@escape(name)}</a>"