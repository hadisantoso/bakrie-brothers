@BBApp.module "Languages", (Languages, App, Backbone, Marionette, $, _) ->
  
  #Usage in .eco files: <%= @translate("nav.top.about_us") %>

  Languages.getIndonesian = ->
    return {
      "general": {
        "download": "Unduh",
        "download_report": "Unduh Laporan",
        "view": "Lihat",
        "view_all_report": "Lihat Laporan Lainnya",
        "view_all": "Lihat Lainnya",
        "read_more": "Baca Selengkapnya",
        "our_company": "Perusahaan Kami",
        "release_date": "Hari Penerbitan",
        "title": "Judul",
        "news_highlight": "Berita Utama",
        "latest_news": "Berita Terkini",
        "more": "lainnya",
        "terms_conditions": "Syarat & Ketentuan"
      },
      "nav": {
        "about_us": {
          "about_us": "Tentang Kami",
          "corporate_history": "Sejarah Perusahaan",
          "download_corporate_history": "Unduh Sejarah Perusahaan",
          "organizations": "Organisasi",
          "vision_mission": " Visi dan Misi",
          "corporate_structure": "Struktur Perusahaan",
          "management":  {
            "management": "Manajemen",
            "commissioners": "Dewan Komisaris",
            "directors": "Direksi dan Manajemen",
            "corporate_secretary": "Sekretaris Perusahaan"
          }
        },
        "our_business": {
          "our_business": "Bisnis",
          "autoparts_industry": "Industri Komponen Otomotif",
          "building_materials_industry": "Industri Bahan Bangunan",
          "infrastructure_projects": "Proyek Infrastruktur",
          "investments": "Investasi",
          "metal_industries": "Industri Metal"
        },
        "corporate_governance": {
          "corporate_governance": "Tata Kelola",
          "gcg_policy": "Kebijakan Tata Kelola",
          "supporting_documents": "Dokumen Pendukung"
        },
        "investor_relations": {
          "investor_relations": "Hubungan Investor",
          "financial_highlights": "Ikhtisar Keuangan",
          "ownership_structure": "Struktur Kepemilikan",
          "company_presentation": " Presentasi Perusahaan",
          "annual_report": "Laporan Tahunan",
          "financial_report": "Laporan Keuangan"

        },
        "sustainability": {
          "sustainability": "Keberlanjutan",
          "csr": "CSR",
          "hse": "HSE"
        },
        "announcements": {
          "announcements": "Pengumuman",
          "gms": {
            "short": "RUPS",
            "long": "Rapat Umum Pemegang Saham"
          },
          "public_expose": "Paparan Publik & Aksi Korporasi",
          "correspondence": "Korespondensi",
          "supporting_documents": "Dokumen Pendukung"
        },
        "human_resources": {
          "human_resources": "SDM",
          "employee_data": "Data Pegawai",
          "career_opportunities": "Peluang Karir",
          "hr_policy": "Kebijakan SDM",
          "training_and_development": "Pelatihan & Pengembangan"
        },
        "news": {
          "news": "Berita",
          "press_release": "Siaran Pers",
          "corporate_news": "Berita Perusahaan"
        }
      },

      "home": {
        "investor": "HUBUNGAN",
        "relations": "Investor",
        "annual": "Tahunan",
        "report": "Laporan",
        "financial": "Keuangan",
        "annual_report_top": "Laporan",
        "annual_report_bottom": "Tahunan",        
        "financial_report_top": "Laporan",
        "financial_report_bottom": "Keuangan",
        "metal": "Metal",
        "auto": "Komponen Otomotif",
        "building": "Bahan Bangunan",
        "infrastructure": "Infrastruktur",
        "investment": "Investasi"
      }
    }