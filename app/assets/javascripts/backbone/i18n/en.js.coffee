@BBApp.module "Languages", (Languages, App, Backbone, Marionette, $, _) ->
  
  #Usage in .eco files: <%= @translate("nav.top.about_us") %>

  Languages.getEnglish = ->
    return {
      "general": {
        "download": "Download",
        "download_report": "Download Report",
        "view": "View",
        "view_all_report": "View All Reports",
        "view_all": "View All",
        "read_more": "Read More",
        "our_company": "Our Company",
        "release_date": "Release Date",
        "title": "Title",
        "news_highlight": "News Highlight",
        "latest_news": "Latest News",
        "more": "more",
        "terms_conditions": "Terms & Conditions"
      },
      "nav": {
        "about_us": {
          "about_us": "About Us",
          "corporate_history": "Corporate History",
          "download_corporate_history": "Download Corporate History",
          "organizations": "Organizations",
          "vision_mission": "Vision and Mission",
          "corporate_structure": "Corporate Structure",
          "management":  {
            "management": "Management",
            "commissioners": "Commissioners",
            "directors": "Directors & Management",
            "corporate_secretary": "Corporate Secretary"
          }
        },
        "our_business": {
          "our_business": "Our Business",
          "autoparts_industry": "Autoparts Industry",
          "building_materials_industry": "Building Materials Industry",
          "infrastructure_projects": "Infrastructure Projects",
          "investments": "Investments",
          "metal_industries": "Metal Industry"
        },
        "corporate_governance": {
          "corporate_governance": "Corporate Governance",
          "gcg_policy": "GCG Policy",
          "supporting_documents": "Supporting Documents"
        },
        "investor_relations": {
          "investor_relations": "Investor Relations",
          "financial_highlights": "Financial Highlights",
          "ownership_structure": "Ownership Structure",
          "company_presentation": "Company Presentation",
          "annual_report": "Annual Report",
          "financial_report": "Financial Report"

        },
        "sustainability": {
          "sustainability": "Sustainability",
          "csr": "CSR",
          "hse": "HSE"
        },
        "announcements": {
          "announcements": "Announcements",
          "gms": {
            "short": "GMS",
            "long": "General Meeting of Shareholders"
          },
          "public_expose": "Public Expose & Corporate Action",
          "correspondence": "Correspondence",
          "supporting_documents": "Supporting Documents"
        },
        "human_resources": {
          "human_resources": "HR",
          "employee_data": "Employee Data",
          "career_opportunities": "Career Opportunities",
          "hr_policy": "HR Policy",
          "training_and_development": "Training & Development"
        },
        "news": {
          "news": "News",
          "press_release": "Press Release",
          "corporate_news": "Corporate News"
        }
      },

      "home": {
        "investor": "INVESTOR",
        "relations": "Relations",
        "annual": "Annual",
        "report": "Report",
        "financial": "Financial",
        "annual_report_top": "Annual",
        "annual_report_bottom": "Report",
        "financial_report_top": "Financial",
        "financial_report_bottom": "Report",
        "metal": "Metal",
        "auto": "Auto Parts",
        "building": "Building Materials",
        "infrastructure": "Infrastructure",
        "investment": "Investment"
      }
    }