@BBApp.module "Languages", (Languages, App, Backbone, Marionette, $, _) ->
  
  Languages.init = (language) ->
      @polyglot = new Polyglot()
      if language == "en"
        map = Languages.getEnglish()
      else if language == "id"
        map = Languages.getIndonesian()

      @polyglot.extend(
        map
      )

  Languages.translate = (name) ->
    if @polyglot == null || @polyglot == undefined
      @init(App.language)
    return @polyglot.t(name)