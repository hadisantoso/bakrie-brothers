@BBApp.module "AuthApp.Login", (Login, App, Backbone, Marionette, $, _) ->
	
  class Login.Layout extends App.Views.Layout
    template: "auth/login/login_layout"
    
    regions:
      titleRegion:  "#title-region"
      formRegion:   "#form-region"
  
  class Login.User extends App.Views.ItemView
    template: "auth/login/login"

    form:
      buttons:
        cancel: false
        primary: "Login"

  class Login.Title extends App.Views.ItemView
    template: "auth/login/login_title"
    
    modelEvents:
      "updated" : "render"