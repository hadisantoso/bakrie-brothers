@BBApp.module "AuthApp.Login", (Login, App, Backbone, Marionette, $, _) ->
	
	class Login.Controller extends App.Controllers.Base
		
		initialize: ->
			auth = App.request "new:auth:entity"
			
			#@listenTo user, "created", ->
			#	App.vent.trigger "user:created", user

			@listenTo auth, "created", ->
				#console.log "auth completed"
				#console.log "auth token: #{auth.get('token')}"
				token = auth.get('token')
				if token?
					toastr.success("login successful")
					App.vent.trigger "login:successful", auth
				else
					toastr.error("The username or password is invalid")
					App.vent.trigger "login:failed", auth
			
			@layout = @getLayoutView()
			
			@listenTo @layout, "show", =>
				console.log "layout show"
				@titleRegion()
				@formRegion(auth)
		
			@show @layout

		titleRegion: ->
			titleView = @getTitleView()
			console.log titleView
			@layout.titleRegion.show titleView
		
		formRegion: (auth) ->
			loginView = @getLoginView(auth)
						
			formView = App.request "form:wrapper", loginView
			
			@layout.formRegion.show formView

		getLayoutView: ->
			new Login.Layout()

		getTitleView: ->
			new Login.Title()

		getLoginView: (auth) ->
			new Login.User
				model: auth