@BBApp.module "AuthApp", (AuthApp, App, Backbone, Marionette, $, _) ->
	
	class AuthApp.Router extends Marionette.AppRouter
		appRoutes:
			"login"	: "login"
	
	loggedIn = false

	API =
		login: ->
			new AuthApp.Login.Controller

	App.addInitializer ->
		new AuthApp.Router
			controller: API