@BBApp.module "EndUser.FooterApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Footer extends App.Views.ItemView
    template: "end_user/footer/show/footer"
    events:
      "click .btn_language_indo": (e) ->
        e.preventDefault()
        App.language = "id"
        BBApp.Languages.init(App.language)
        App.execute "home:start"
        App.setupEndUser()
        #App.navigate("/")
        Cookies.set('language', "id" , { expires: 1 })
        
        App.scrollToTop()

      "click .btn_language_en": (e) ->
        e.preventDefault()
        App.language = "en"
        BBApp.Languages.init(App.language)
        App.execute "home:start"
        App.setupEndUser()
        #App.navigate("/")
        Cookies.set('language', "en" , { expires: 1 })

        App.scrollToTop()