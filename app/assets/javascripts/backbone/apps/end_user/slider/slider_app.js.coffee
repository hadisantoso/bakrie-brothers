@BBApp.module "EndUser.SliderApp", (SliderApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting slider app"
      new SliderApp.Show.Controller
        region: region

  SliderApp.on "start", (options) ->
    API.start(options.region)