@BBApp.module "EndUser.SliderApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/slider/show/layout"

    regions:
      slidesRegion: "#slides-region"

  class Show.Slide extends App.Views.ItemView
    template: "end_user/slider/show/slide"
    className: "item"

  class Show.Slides extends App.Views.CompositeView
    template: "end_user/slider/show/slides"
    childView: Show.Slide
    childViewContainer: ".slides"