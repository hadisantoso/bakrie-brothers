@BBApp.module "EndUser.SliderApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing SliderApp.Show.Controller"

      @layout = @getLayoutView()
      @region.show @layout

      slides = App.request "end_user:post:get", ("HomeImagePost")

      @listenTo slides, "sync:stop", ->
        console.log "starting carousel"
        $(".item").first().addClass("active")
        $('.carousel').carousel()

      view = new Show.Slides
        collection: slides

      @layout.slidesRegion.show view

    getLayoutView: ->
      new Show.Layout()