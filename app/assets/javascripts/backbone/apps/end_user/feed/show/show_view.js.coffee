@BBApp.module "EndUser.FeedApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/feed/show/layout"

    regions:
      highlightRegion:  "#highlight-region"
      newsRegion:  "#news-region"

  class Show.Highlight extends App.Views.ItemView
    template: "end_user/feed/show/post"

  class Show.Highlights extends App.Views.CompositeView
    template: "end_user/feed/show/posts"
    childView: Show.Highlight
    childViewContainer: "div"

  class Show.Post extends App.Views.ItemView
    template: "end_user/feed/show/post"

  class Show.Posts extends App.Views.CompositeView
    template: "end_user/feed/show/posts"
    childView: Show.Post
    childViewContainer: "div"