@BBApp.module "EndUser.FeedApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing FeedApp.Show.Controller"
      highlights = App.request "end_user:post:latest", ("NewsPost")
      news = App.request "end_user:post:latest", ("PrPost")

      @layout = @getLayoutView()
      @region.show @layout

      @highlightRegion(highlights)
      @newsRegion(news)

    getLayoutView: ->
      new Show.Layout()

    highlightRegion: (highlights) ->
      highlightView = @getHighlightView(highlights)
      
      @layout.highlightRegion.show highlightView

    getHighlightView: (highlights) ->
      new Show.Highlights
        collection: highlights

    newsRegion: (posts) ->
      newsView = @getNewsView(posts)
      
      @layout.newsRegion.show newsView

    getNewsView: (posts) ->
      new Show.Highlights
        collection: posts