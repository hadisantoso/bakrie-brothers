@BBApp.module "EndUser.FeedApp", (FeedApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting feed app"
      new FeedApp.Show.Controller
        region: region

  FeedApp.on "start", (options) ->
    API.start(options.region)