@BBApp.module "EndUser.HeaderApp", (HeaderApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  API =
    list: (region) ->
      new HeaderApp.List.Controller
        region: region
  
  HeaderApp.on "start", (options) ->
    API.list(options.region)