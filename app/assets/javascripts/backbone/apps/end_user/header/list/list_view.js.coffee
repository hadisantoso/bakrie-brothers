@BBApp.module "EndUser.HeaderApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.Header extends App.Views.ItemView
    template: "end_user/header/list/header"
    events:
      "click a.top": (e) ->
        li = $(e.target).parent()
        $("li.dropdown").removeClass("active")
        li.addClass("active")

      "click .btn_language_indo": (e) ->
        e.preventDefault()
        App.language = "id"
        BBApp.Languages.init(App.language)
        
        App.execute "home:start"

        fragment = Backbone.history.getFragment()
        Backbone.history.loadUrl(fragment)

        App.setupEndUser()
        Cookies.set('language', "id" , { expires: 1 });
        
        App.scrollToTop()

      "click .btn_language_en": (e) ->
        e.preventDefault()
        App.language = "en"
        BBApp.Languages.init(App.language)
        
        App.execute "home:start"
        fragment = Backbone.history.getFragment()
        Backbone.history.loadUrl(fragment)
        
        App.setupEndUser()
        Cookies.set('language', "en" , { expires: 1 })
        
        App.scrollToTop()