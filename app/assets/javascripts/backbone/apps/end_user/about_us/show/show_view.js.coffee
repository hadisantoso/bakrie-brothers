@BBApp.module "EndUser.AboutUsApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/about_us/show/layout"

    regions:
      aboutUsRegion: "#about-us-region"
      corporateHistoryRegion: "#corporate-history-region"


  class Show.PageSummary extends App.Views.ItemView
    template:"end_user/about_us/show/page_summary"

  class Show.PageSummaries extends App.Views.CompositeView
    template:"end_user/about_us/show/page_summaries"
    childView: Show.PageSummary