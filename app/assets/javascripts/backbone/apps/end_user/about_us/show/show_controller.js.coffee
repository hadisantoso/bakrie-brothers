@BBApp.module "EndUser.AboutUsApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing AboutUsApp.Show.Controller"
      layout = @getLayoutView()
      @region.show layout

      aboutUs = App.request "end_user:post:page:get", "about us"
      aboutUsView = new Show.PageSummaries
        collection: aboutUs

      corporateHistory = App.request "end_user:post:page:get", "corporate history"
      corporateHistoryView = new Show.PageSummaries
        collection: corporateHistory

      layout.aboutUsRegion.show aboutUsView
      layout.corporateHistoryRegion.show corporateHistoryView

    getLayoutView: ->
      new Show.Layout()