@BBApp.module "EndUser.AboutUsApp", (AboutUsApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting about us app"
      new AboutUsApp.Show.Controller
        region: region

  AboutUsApp.on "start", (options) ->
    API.start(options.region)