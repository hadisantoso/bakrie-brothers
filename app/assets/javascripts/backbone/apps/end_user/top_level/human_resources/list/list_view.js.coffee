@BBApp.module "EndUser.TopLevel.HumanResourcesApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.HumanResourcesLayout extends App.Views.Layout
    template: "end_user/top_level/human_resources/list/human_resources"

    regions:
      humanResourcesRegion:  "#human-resources-region"

    onRender: ->
      $(document).scrollTop(0)