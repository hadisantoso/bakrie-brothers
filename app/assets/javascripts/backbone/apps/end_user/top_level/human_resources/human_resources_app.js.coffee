@BBApp.module "EndUser.TopLevel.HumanResourcesApp", (HumanResourcesApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class HumanResourcesApp.Router extends Marionette.AppRouter
    appRoutes:
      "human_resources(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level HumanResourcesApp: #{path}"
      new HumanResourcesApp.List.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level HumanResourcesApp addInitializer"
    new HumanResourcesApp.Router
      controller: API