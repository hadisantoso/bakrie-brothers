@BBApp.module "EndUser.TopLevel.HumanResourcesApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing HumanResourcesApp.List.Controller"
      @layout = @getLayout()
      @region.show @layout

      @showView(options.path, @layout.humanResourcesRegion)

    getLayout: ->
      new List.HumanResourcesLayout()

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    showView: (path, region) ->
      console.log "Path: #{path}"
      if path == "" || path == "data_pegawai"
        App.request "page:display", "employee data", region, @cb
      else if path == "peluang_karir"
        App.request "page:display", "career opportunities", region, @cb
      else if path == "kebijakan_sdm"
        App.request "page:display", "hr policy", region, @cb
      else if path == "pelatihan_dan_pengembangan"
        App.request "page:display", "training_and_development", region, @cb