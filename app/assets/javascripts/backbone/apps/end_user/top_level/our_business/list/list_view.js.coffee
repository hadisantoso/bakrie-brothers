@BBApp.module "EndUser.TopLevel.OurBusinessApp.Show", (Show, App, Backbone, Marionette, $, _) ->

  class Show.Layout extends App.Views.Layout
    template: "end_user/top_level/our_business/list/layout"

    regions:
      ourBusinessRegion:  "#our-business-region"

    onRender: ->
      $(document).scrollTop(0)
