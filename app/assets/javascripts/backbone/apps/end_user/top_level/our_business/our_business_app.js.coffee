@BBApp.module "EndUser.TopLevel.OurBusinessApp", (OurBusinessApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class OurBusinessApp.Router extends Marionette.AppRouter
    appRoutes:
      "our_business(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level our business us app: #{path}"
      new OurBusinessApp.Show.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level our business app addInitializer"
    new OurBusinessApp.Router
      controller: API