@BBApp.module "EndUser.TopLevel.OurBusinessApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      @layout = @getLayout()      
      @region.show @layout
      @show(options.path)

    getLayout: ->
      new Show.Layout()

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    show: (path) ->
      if path == ""
        App.request "page:display", "our business", @layout.ourBusinessRegion, @cb
      else if path == "autoparts_industry"
        App.request "page:display", "autoparts industry", @layout.ourBusinessRegion, @cb
      else if path == "building_materials_industry"
        App.request "page:display", "building materials industry", @layout.ourBusinessRegion, @cb
      else if path == "infrastructure_projects"
        App.request "page:display", "infrastructure projects", @layout.ourBusinessRegion, @cb
      else if path == "investments"
        App.request "page:display", "investments", @layout.ourBusinessRegion, @cb
      else if path == "metal_industries"
        App.request "page:display", "metal industries", @layout.ourBusinessRegion, @cb