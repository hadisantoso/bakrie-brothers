@BBApp.module "EndUser.TopLevel.InvestorRelationsApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing InvestorRelationsApp.List.Controller"
      @layout = @getLayout()
      @region.show @layout
      @show(options.path)      

    getLayout: ->
      new List.Layout()

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    show: (path) ->
      console.log "Path: #{path}"
      if path == ""
        App.request "page:display", "investor relations", @layout.region, @cb

      else if path == "financial_highlight"      
        App.request "page:display", "financial highlight", @layout.region, @cb

      else if path == "ownership_structure"
        App.request "page:display", "ownership structure", @layout.region, @cb

      else if path == "company_presentation"
        App.request "document:table", "title", "company_presentations", "CompanyPresentationPost", @layout.region

      else if path == "annual_report"
        App.request "document:table", "title", "annual_reports", "AnnualReportPost", @layout.region

      else if path == "financial_report"        
        App.request "document:table", "title", "financial_reports", "FinancialReportPost", @layout.region