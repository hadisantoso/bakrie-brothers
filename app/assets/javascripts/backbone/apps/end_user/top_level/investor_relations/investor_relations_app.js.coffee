@BBApp.module "EndUser.TopLevel.InvestorRelationsApp", (InvestorRelationsApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class InvestorRelationsApp.Router extends Marionette.AppRouter
    appRoutes:
      "investor_relations(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level InvestorRelationsApp: #{path}"
      new InvestorRelationsApp.List.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level InvestorRelationsApp addInitializer"
    new InvestorRelationsApp.Router
      controller: API