@BBApp.module "EndUser.TopLevel.InvestorRelationsApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.Layout extends App.Views.Layout
    template: "end_user/top_level/investor_relations/list/layout"

    regions:
      region:  "#region"

    onRender: ->
      $(document).scrollTop(0)