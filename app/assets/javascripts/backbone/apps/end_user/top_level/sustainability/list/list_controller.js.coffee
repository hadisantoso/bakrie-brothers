@BBApp.module "EndUser.TopLevel.SustainabilityApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing SustainabilityApp.List.Controller"
      @layout = @getLayout()
      @innerLayout = @getInnerLayout(options.path)
      @region.show @layout
      @layout.region.show @innerLayout

      @showInnerLayoutContent(options.path, @innerLayout.contentRegion)

      App.request "article:list", "", "", @getPostType(options.path), @innerLayout.feedRegion

    getLayout: ->
      new List.SustainabilityLayout()

    getInnerLayout: (path) ->
      if path == "" || path == "csr"
        new List.CSRLayout()
      else if path == "hse"
        new List.HSELayout()

    getPostType: (path) ->
      if path == "" || path == "csr"
        "CsrPost"
      else if path == "hse"
        "HsePost"

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    showInnerLayoutContent: (path, region) ->
      if path == "" || path == "csr"
        App.request "page:display", "csr", region, @cb
      else if path == "hse"
        App.request "page:display", "hse", region, @cb
      