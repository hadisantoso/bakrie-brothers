@BBApp.module "EndUser.TopLevel.SustainabilityApp", (SustainabilityApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class SustainabilityApp.Router extends Marionette.AppRouter
    appRoutes:
      "sustainability(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level SustainabilityApp: #{path}"
      new SustainabilityApp.List.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level SustainabilityApp addInitializer"
    new SustainabilityApp.Router
      controller: API