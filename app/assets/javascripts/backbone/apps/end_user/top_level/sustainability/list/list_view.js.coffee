@BBApp.module "EndUser.TopLevel.SustainabilityApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.SustainabilityLayout extends App.Views.Layout
    template: "end_user/top_level/sustainability/list/sustainability"

    regions:
      region:  "#region"

    onRender: ->
      $(document).scrollTop(0)

  class List.CSRLayout extends App.Views.Layout
    template: "end_user/top_level/sustainability/list/csr_layout"

    regions:
      feedRegion: "#feed-region"
      contentRegion: "#content-region"

  class List.HSELayout extends App.Views.Layout
    template: "end_user/top_level/sustainability/list/hse_layout"

    regions:
      feedRegion: "#feed-region"
      contentRegion: "#content-region"