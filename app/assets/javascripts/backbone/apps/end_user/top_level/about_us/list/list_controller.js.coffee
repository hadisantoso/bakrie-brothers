@BBApp.module "EndUser.TopLevel.AboutUsApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing AboutUsApp.Show.Controller"

      @layout = @getLayout()
      @region.show @layout
      @show(options.path)

      App.vent.unbind "profile:clicked"
      App.vent.on "profile:clicked", (profile) =>
        detailedProfile = new Show.DetailedProfile
            model: profile
        @view.detailedProfileRegion.show detailedProfile

    getLayout: ->
      new Show.Layout()

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    show: (path) ->
      if path == ""
        App.request "page:display", "about us", @layout.aboutUsRegion, @cb
        return
      else if path == "corporate_history"
        corporateHistoryLayout = new Show.CorporateHistory()
        @layout.aboutUsRegion.show corporateHistoryLayout
        
        App.request "page:display", "corporate history", corporateHistoryLayout.corporateHistoryIntroRegion, @cb
        App.request "timeline:list", "", "CorporateHistoryPost", corporateHistoryLayout.corporateHistoryTimelineRegion

        return
      else if path == "organizations"
        App.request "page:display", "organizations", @layout.aboutUsRegion, @cb
        return
      else if path == "vision_mission"
        App.request "page:display", "vision mission", @layout.aboutUsRegion, @cb
        return
      else if path == "corporate_structure"
        App.request "page:display", "corporate structure", @layout.aboutUsRegion, @cb
        return
      else if path == "management"
        commissioners = App.request "end_user:post:get", ("CommissionerPost")
        directors = App.request "end_user:post:get", ("DirectorPost")
        corporate_secretaries = App.request "end_user:post:get", ("CorporateSecretaryPost")
        managementLayout = new Show.Management()
        @listenTo managementLayout, "show", =>
          profilesCommisioners = new Show.Profiles
            collection: commissioners
          profilesDirectors = new Show.Profiles
            collection: directors
          profilesCorporateSecretaries = new Show.Profiles
            collection: corporate_secretaries
          managementLayout.commissionersRegion.show profilesCommisioners
          managementLayout.directorsRegion.show profilesDirectors
          managementLayout.corporateSecretariesRegion.show profilesCorporateSecretaries
          App.request "page:display", "management", managementLayout.managementRegion, @cb
        @view = managementLayout
      
      @layout.aboutUsRegion.show @view
