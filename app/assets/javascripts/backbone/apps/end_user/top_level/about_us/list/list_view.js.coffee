@BBApp.module "EndUser.TopLevel.AboutUsApp.Show", (Show, App, Backbone, Marionette, $, _) ->

  class Show.Layout extends App.Views.Layout
    template: "end_user/top_level/about_us/list/layout"

    regions:
      aboutUsRegion:  "#about-us-region"

    onRender: ->
      $(document).scrollTop(0)

  class Show.TermsLayout extends App.Views.Layout
    template: "end_user/top_level/about_us/list/terms_layout"

    regions:
      termsRegion:  "#terms-region"

    onRender: ->
      $(document).scrollTop(0)

  class Show.CorporateHistory extends App.Views.Layout
    template: "end_user/top_level/about_us/list/corporate_history"
    regions:
      corporateHistoryIntroRegion: "#corporate-history-intro"
      corporateHistoryTimelineRegion: "#corporate-history-timeline"



  class Show.Management extends App.Views.Layout
    template: "end_user/top_level/about_us/list/management"

    regions:
      commissionersRegion: "#commissioners-region"
      directorsRegion: "#directors-region"
      corporateSecretariesRegion: "#corporate-secretaries-region"
      detailedProfileRegion: "#detailed-profile-region"
      managementRegion: "#management-region"
      

  class Show.Profile extends App.Views.ItemView
    template: "end_user/top_level/about_us/list/profile"

    events:
      "click .profile": ->
        App.vent.trigger "profile:clicked", @model

  
  class Show.Profiles extends App.Views.CompositeView
    template: "end_user/top_level/about_us/list/profiles"
    childView: Show.Profile
    childViewContainer: ".profiles"

    onRender: ->
      @$('.fancybox').fancybox();

  class Show.DetailedProfile extends App.Views.ItemView
    template: "end_user/top_level/about_us/list/detailed_profile"