@BBApp.module "EndUser.TopLevel.AboutUsApp", (AboutUsApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class AboutUsApp.Router extends Marionette.AppRouter
    appRoutes:
      "about_us(/:path)" : "start"
      "terms" : "terms"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level about us app: #{path}"
      new AboutUsApp.Show.Controller
        region: App.mainRegion
        path: path

    terms: ->
      App.setupEndUser()
      termsLayout = new AboutUsApp.Show.TermsLayout()
      App.mainRegion.show termsLayout
      App.request "page:display", "terms_conditions", termsLayout.termsRegion, (pagePost) ->
        termsLayout.showHeader(pagePost.get("header_image").original_resolution)

  App.addInitializer ->
    console.log "end user top level about us app addInitializer"
    new AboutUsApp.Router
      controller: API