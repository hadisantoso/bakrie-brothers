@BBApp.module "EndUser.TopLevel.CorporateGovernanceApp.Show", (Show, App, Backbone, Marionette, $, _) ->

  class Show.CorporateGovernanceLayout extends App.Views.Layout
    template: "end_user/top_level/corporate_governance/show/layout"

    regions:
      corporateGovernanceRegion:  "#corporate_governance_region"
      region:  "#region"

    onRender: ->
      $(document).scrollTop(0)