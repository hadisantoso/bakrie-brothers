@BBApp.module "EndUser.TopLevel.CorporateGovernanceApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing CorporateGovernanceApp.Show.Controller"
      @layout = @getLayout()
      @region.show @layout
      @show(options.path)
      

    getLayout: ->
      new Show.CorporateGovernanceLayout()

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

    show: (path) ->
      if path == "" || path == "gcg_policy"
        App.request "page:display", "corporate governance", @layout.corporateGovernanceRegion, @cb        
        App.request "accordion:list", "", "CorporateGovernancePost", @layout.region
      else if path == "supporting_documents"
        App.request "document:table", "title", "corporate_governance_documents", "CorporateGovernanceDocumentPost", @layout.region