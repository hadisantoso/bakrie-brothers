@BBApp.module "EndUser.TopLevel.CorporateGovernanceApp", (CorporateGovernanceApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class CorporateGovernanceApp.Router extends Marionette.AppRouter
    appRoutes:
      "corporate_governance(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level corporate_governance us app: #{path}"
      new CorporateGovernanceApp.Show.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level corporate governance app addInitializer"
    new CorporateGovernanceApp.Router
      controller: API