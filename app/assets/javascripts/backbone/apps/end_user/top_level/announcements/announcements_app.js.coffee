@BBApp.module "EndUser.TopLevel.AnnouncementsApp", (AnnouncementsApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class AnnouncementsApp.Router extends Marionette.AppRouter
    appRoutes:
      "announcements(/:path)" : "start"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level AnnouncementsApp: #{path}"
      new AnnouncementsApp.List.Controller
        region: App.mainRegion
        path: path

  App.addInitializer ->
    console.log "end user top level AnnouncementsApp addInitializer"
    new AnnouncementsApp.Router
      controller: API