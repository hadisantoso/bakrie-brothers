@BBApp.module "EndUser.TopLevel.AnnouncementsApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.Layout extends App.Views.Layout
    template: "end_user/top_level/announcements/list/layout"

    regions:
      region:  "#region"

    onRender: ->
      $(document).scrollTop(0)