@BBApp.module "EndUser.TopLevel.AnnouncementsApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      path = options.path
      @layout = new List.Layout()
      
      post_type = @getPostType(path)
      page = @getPage(path)

      options.region.show @layout

      App.request "document:table", "title", page, post_type, @layout.region


    getPage: (path) ->
      if path == "" || path == "rups"
        return "rups"
      else if path == "public_expose"
        return "public expose"
      else if path == "correspondence"
        return "correspondence"
      else if path == "supporting_documents"
        return "announcement_documents"

      

    getPostType: (path) ->
      if path == "" || path == "rups"
        return "RupsPost"        
      else if path == "public_expose"
        return "PublicExposePost"
      else if path == "correspondence"
        return "CorrespondencePost"
      else if path == "supporting_documents"
        return "AnnouncementDocumentPost"