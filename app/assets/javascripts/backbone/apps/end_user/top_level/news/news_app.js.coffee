@BBApp.module "EndUser.TopLevel.NewsApp", (NewsApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class NewsApp.Router extends Marionette.AppRouter
    appRoutes:
      "news(/:path)" : "start"
      "article(/:id)" : "showArticle"

  API =
    start: (path) ->
      App.setupEndUser()
      if path == null
        path = ""
      console.log "starting end user top level NewsApp: #{path}"
      new NewsApp.List.Controller
        region: App.mainRegion
        path: path

    showArticle: (id) ->
      App.setupEndUser()
      if id == null
        id = ""
      console.log "starting end user top level NewsApp: #{id}"
      new NewsApp.List.Controller2
        region: App.mainRegion
        id: id


  App.addInitializer ->
    console.log "end user top level NewsApp addInitializer"
    new NewsApp.Router
      controller: API