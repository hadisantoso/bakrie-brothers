@BBApp.module "EndUser.TopLevel.NewsApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      @path = options.path
      @layout = @getLayout()
      @region.show @layout
      App.request "article:list", @getTitle(@path), @getPagePost(@path), @getPostType(@path), @layout.region

    getLayout: ->
      new List.Layout()

    getPostType: (path) ->
      if path == "" || path == "press_release"
        return "PrPost"
      else if path == "corporate_news"
        return "NewsPost"
      return ""

    getTitle: (path) ->
      if path == "" || path == "press_release"
        return App.Languages.translate("nav.news.press_release")
      else if path == "corporate_news"
        return App.Languages.translate("nav.news.corporate_news")
      return ""

    getPagePost: (path) ->
      if path == ""
        return "press_release"
      else 
        return path

        
  class List.Controller2 extends App.Controllers.Base
    
    initialize: (options) ->
      @layout = @getLayout()

      post = App.request "end_user:post:entity", options.id

      App.execute "when:fetched", post, =>
        view = new List.ExpandedPost
          model: post

        @region.show @layout
        @layout.region.show view

    getLayout: ->
      new List.Layout()
