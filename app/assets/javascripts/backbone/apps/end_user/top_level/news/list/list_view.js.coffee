@BBApp.module "EndUser.TopLevel.NewsApp.List", (List, App, Backbone, Marionette, $, _) ->

  class List.Layout extends App.Views.Layout
    template: "end_user/top_level/news/list/layout"

    regions:
      region:  "#news-region"

    onRender: ->
      $(document).scrollTop(0)

  class List.ExpandedPost extends App.Views.ItemView
    template: "end_user/top_level/news/list/expanded_post"    