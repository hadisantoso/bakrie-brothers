@BBApp.module "EndUser.FinanceApp", (FinanceApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting finance app"
      new FinanceApp.Show.Controller
        region: region

  FinanceApp.on "start", (options) ->
    API.start(options.region)