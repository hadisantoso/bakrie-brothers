@BBApp.module "EndUser.FinanceApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing FinanceApp.Show.Controller"
      annualReport = App.request "end_user:post:latest", ("AnnualReportPost")
      annualReportView = new Show.AnnualReports
        collection: annualReport

      financialReports = App.request "end_user:post:latestN", "FinancialReportPost", 3      
      financialReportView = new Show.FinancialReports
        collection: financialReports

      @layout = @getLayoutView()
      @region.show @layout
      @layout.annualReportRegion.show annualReportView
      @layout.financialReportRegion.show financialReportView

    getLayoutView: ->
      new Show.Layout()