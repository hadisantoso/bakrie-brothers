@BBApp.module "EndUser.FinanceApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/finance/show/layout"

    regions:
      annualReportRegion: "#annual-report-region"
      financialReportRegion: "#financial-report-region"

  class Show.AnnualReport extends App.Views.ItemView
    template: "end_user/finance/show/annual_report"

  class Show.AnnualReports extends App.Views.CompositeView
    template: "end_user/finance/show/annual_reports"
    childView: Show.AnnualReport

  class Show.FinancialReport extends App.Views.ItemView
    template: "end_user/finance/show/financial_report"

  class Show.FinancialReports extends App.Views.CompositeView
    template: "end_user/finance/show/financial_reports"
    childView: Show.FinancialReport