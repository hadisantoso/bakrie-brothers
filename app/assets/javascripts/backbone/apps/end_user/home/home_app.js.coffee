@BBApp.module "EndUser.HomeApp", (HomeApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class HomeApp.Router extends Marionette.AppRouter
    appRoutes:
      "" : "start"
      "home" : "start"

  API =
    start: ->
      console.log "starting home app"
      new HomeApp.Show.Controller
        region: App.mainRegion

  App.addInitializer ->
    console.log "homeapp addInitializer"
    new HomeApp.Router
      controller: API

  App.commands.setHandler "home:start", ->
    API.start()