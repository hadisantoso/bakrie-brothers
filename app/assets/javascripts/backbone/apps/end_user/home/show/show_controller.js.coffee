@BBApp.module "EndUser.HomeApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing HomeApp.Show.Controller"
      @layout = @getLayoutView()

      @listenTo @layout, "show", =>
        App.startModule("EndUser.HeaderApp", App.headerRegion)
        App.startModule("EndUser.SliderApp", @layout.sliderRegion)
        App.startModule("EndUser.AboutUsApp", @layout.aboutUsRegion)
        App.startModule("EndUser.ServicesApp", @layout.servicesRegion)
        App.startModule("EndUser.FinanceApp", @layout.financeRegion)
        App.startModule("EndUser.TickerApp", @layout.tickerRegion)
        App.startModule("EndUser.FeedApp", @layout.feedRegion)
        App.startModule("EndUser.FooterApp", App.footerRegion)

      @region.show @layout

    getLayoutView: ->
      new Show.Layout()