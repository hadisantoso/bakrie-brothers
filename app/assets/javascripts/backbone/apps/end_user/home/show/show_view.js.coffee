@BBApp.module "EndUser.HomeApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/home/show/layout"

    regions:
      headerRegion:  "#header-region"
      sliderRegion:  "#slider-region"
      aboutUsRegion: "#about-us-region"
      servicesRegion:"#services-region"  
      financeRegion: "#finance-region"
      tickerRegion:  "#ticker-region"      
      feedRegion:    "#feed-region"      
      footerRegion: "#footer-region"