@BBApp.module "EndUser.ServicesApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/services/show/layout"