@BBApp.module "EndUser.ServicesApp", (ServicesApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting services app"
      new ServicesApp.Show.Controller
        region: region

  ServicesApp.on "start", (options) ->
    API.start(options.region)