@BBApp.module "EndUser.TickerApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "end_user/ticker/show/layout"