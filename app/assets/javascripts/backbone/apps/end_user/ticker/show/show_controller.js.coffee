@BBApp.module "EndUser.TickerApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing TickerApp.Show.Controller"
      @layout = @getLayoutView()
      @region.show @layout

    getLayoutView: ->
      new Show.Layout()