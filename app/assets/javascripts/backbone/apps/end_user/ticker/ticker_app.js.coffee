@BBApp.module "EndUser.TickerApp", (TickerApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false

  API =
    start: (region) ->
      console.log "starting ticker app"
      new TickerApp.Show.Controller
        region: region

  TickerApp.on "start", (options) ->
    API.start(options.region)