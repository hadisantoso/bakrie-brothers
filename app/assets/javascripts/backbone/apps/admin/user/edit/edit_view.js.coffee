@BBApp.module "UserApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->
	
	class Edit.Layout extends App.Views.Layout
		template: "admin/user/edit/edit_layout"
		
		regions:
			titleRegion:	"#title-region"
			formRegion: 	"#form-region"
	
	class Edit.Title extends App.Views.ItemView
		template: "admin/user/edit/edit_title"
		
		modelEvents:
			"updated" : "render"
	
	class Edit.User extends App.Views.ItemView
		template: "admin/user/edit/edit_user"

		modelEvents:
			"upload:success" : "render"