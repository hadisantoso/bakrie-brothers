@BBApp.module "UserApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->
	
	class Edit.Controller extends App.Controllers.Base
		
		initialize: (options) ->
			{ user, id } = options
			user or= App.request "user:entity", id
			
			@listenTo user, "updated", ->
				App.vent.trigger "user:updated", user

			@listenTo user, "upload:success", ->
				console.log "user upload succeeded"
			
			App.execute "when:fetched", user, =>
				@layout = @getLayoutView user
				
				@listenTo @layout, "show", =>
					@titleRegion user
					@formRegion user
			
				@show @layout

		
		titleRegion: (user) ->
			titleView = @getTitleView user
			@layout.titleRegion.show titleView
		
		formRegion: (user) ->
			editView = @getEditView user
			
			@listenTo editView, "form:cancel", ->
				App.vent.trigger "user:cancelled", user
			
			formView = App.request "form:wrapper", editView, 
				imageUpload: true
			
			@layout.formRegion.show formView
		
		getTitleView: (user) ->
			new Edit.Title
				model: user
		
		getLayoutView: (user) ->
			new Edit.Layout
				model: user
		
		getEditView: (user) ->
			new Edit.User
				model: user