@BBApp.module "UserApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Layout extends App.Views.Layout
		template: "admin/user/list/list_layout"
		
		regions:
			titleRegion: 	"#title-region"
			panelRegion:	"#panel-region"
			newRegion:		"#new-region"
			userRegion:		"#user-region"
	
	class List.Title extends App.Views.ItemView
		template: "admin/user/list/_title"
	
	class List.Panel extends App.Views.ItemView
		template: "admin/user/list/_panel"
		
		triggers:
			"click #new-user" : "new:user:button:clicked"
	
	class List.UserMember extends App.Views.ItemView
		template: "admin/user/list/_user_member"
		tagName: "tr"
		className: "user-member"
			
		triggers:
			"click .user-delete button" : "user:delete:clicked"
			"click" 										: "user:member:clicked"
	
	class List.Empty extends App.Views.ItemView
		template: "admin/user/list/_empty"
		tagName: "tr"
	
	class List.User extends App.Views.CompositeView
		template: "admin/user/list/_user"
		childView: List.UserMember
		emptyView: List.Empty
		childViewContainer: "tbody"