@BBApp.module "UserApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Controller extends App.Controllers.Base
		
		initialize: ->
			user = App.request "user:entities"
			
			App.execute "when:fetched", user, =>
			
				@layout = @getLayoutView user
				
				@listenTo @layout, "close", @close
			

				@listenTo @layout, "show", =>
					console.log "layout onshow"
					@titleRegion()
					@panelRegion()
					@userRegion user
			
				@show @layout
		
		titleRegion: ->
			titleView = @getTitleView()
			@layout.titleRegion.show titleView
		
		panelRegion: ->
			panelView = @getPanelView()
			
			@listenTo panelView, "new:user:button:clicked", =>
				@newRegion()
			
			@layout.panelRegion.show panelView
		
		newRegion: ->
			App.execute "new:user:member", @layout.userRegion
		
		userRegion: (user) ->
			userView = @getUserView user
			
			@listenTo userView, "childview:user:member:clicked", (child, args) ->
				App.vent.trigger "user:member:clicked", args.model
			
			@listenTo userView, "childview:user:delete:clicked", (child, args) ->
				model = args.model
				if confirm "Are you sure you want to delete #{model.get("name")}?" then model.destroy() else false
			
			console.log @layout.userRegion
			@layout.userRegion.show userView
		
		getUserView: (user) ->
			new List.User
				collection: user
		
		getPanelView: ->
			new List.Panel
		
		getTitleView: ->
			new List.Title
		
		getLayoutView: (user) ->
			new List.Layout
				collection: user