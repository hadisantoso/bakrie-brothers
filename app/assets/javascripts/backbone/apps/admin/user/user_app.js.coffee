@BBApp.module "UserApp", (UserApp, App, Backbone, Marionette, $, _) ->
	
	class UserApp.Router extends Marionette.AppRouter
		appRoutes:
			"admin/user/:id/edit"	: "edit"
			"admin/user"					: "list"
	
	API =
		list: ->
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			console.log "userapp.list()"
			new UserApp.List.Controller
		
		newUser: (region) ->
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			new UserApp.New.Controller
				region: region
		
		edit: (id, member) ->
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			new UserApp.Edit.Controller
				id: id
				user: member
	
	App.commands.setHandler "new:user:member", (region) ->
		API.newUser region
	
	App.vent.on "user:member:clicked user:created", (member) ->
		console.log member
		App.navigate Routes.edit_admin_user_path(member.id)
		API.edit member.id, member
	
	App.vent.on "user:cancelled user:updated", (user) ->
		App.navigate Routes.admin_user_index_path()
		API.list()
	
	App.addInitializer ->
		console.log "userapp addInitializer"
		new UserApp.Router
			controller: API