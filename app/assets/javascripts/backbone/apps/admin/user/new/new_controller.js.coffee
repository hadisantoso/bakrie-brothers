@BBApp.module "UserApp.New", (New, App, Backbone, Marionette, $, _) ->
	
	class New.Controller extends App.Controllers.Base
		
		initialize: ->
			user = App.request "new:user:entity"
			
			@listenTo user, "created", ->
				App.vent.trigger "user:created", user
			
			newView = @getNewView user
			formView = App.request "form:wrapper", newView
			
			@listenTo newView, "form:cancel", =>
				@region.reset()
			
			@show formView
		
		getNewView: (user) ->
			new New.User
				model: user