@BBApp.module "UserApp.New", (New, App, Backbone, Marionette, $, _) ->
	
	class New.User extends App.Views.ItemView
		template: "admin/user/new/new_user"
		
		form:
			buttons:
				placement: "left"