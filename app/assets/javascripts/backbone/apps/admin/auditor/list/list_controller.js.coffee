@BBApp.module "AuditorApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      @collection = null
      @page = 1
      view = @getView()
      region = options.region
      region.show view

      #handle venting of pagination's "more"
      App.vent.unbind "audit_record:more"
      App.vent.on "audit_record:more", =>
        console.log "more"
        @page = @page + 1
        if @collection != null && @collection.length > 0
          @collection = App.request "admin:audit_records:get:more", @collection, @page

    getView: =>
      @collection = App.request "admin:audit_records:get"
      view = new List.AuditRecords
        collection: @collection

      return view      