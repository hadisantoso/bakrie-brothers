@BBApp.module "AuditorApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.AuditRecord extends App.Views.ItemView
    template: "admin/auditor/list/audit_record"    
    tagName: "tr"

  class List.AuditRecords extends App.Views.CompositeView
    template: "admin/auditor/list/audit_records"
    childView: List.AuditRecord
    childViewContainer: "tbody"

    collectionEvents:
      "sync:stop": ->
        if @collection.length == 0
          @$(".paging").hide()
        else if @collection.length == @currentLength
          @$(".paging").hide()

    events:
      "click .btn_more": (e) ->
        e.preventDefault()
        @currentLength = @collection.length

        App.vent.trigger "audit_record:more"