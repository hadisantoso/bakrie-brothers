@BBApp.module "AuditorApp", (AuditorApp, App, Backbone, Marionette, $, _) ->
  
  class AuditorApp.Router extends Marionette.AppRouter
    appRoutes:
      "admin/auditor" : "list"
  
  API =
    list: ->
      if !App.validUser()
        App.goToLogin()
        return
      App.setupAdminUser()
      new AuditorApp.List.Controller
        region: App.mainRegion
        
  App.addInitializer ->
    new AuditorApp.Router
      controller: API