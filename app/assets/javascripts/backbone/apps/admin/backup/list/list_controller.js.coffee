@BBApp.module "BackupApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      @collection = null
      @page = 1
      view = @getView()
      region = options.region
      region.show view

    getView: =>
      @collection = App.request "admin:backups:get"
      view = new List.Backups
        collection: @collection

      return view      