@BBApp.module "BackupApp.List", (List, App, Backbone, Marionette, $, _) ->
  
  class List.Empty extends App.Views.ItemView
    template: "admin/backup/list/empty"    
    tagName: "tr"

  class List.Backup extends App.Views.ItemView
    template: "admin/backup/list/backup"    
    tagName: "tr"

  class List.Backups extends App.Views.CompositeView
    template: "admin/backup/list/backups"
    childView: List.Backup
    childViewContainer: "tbody"
    emptyView: List.Empty