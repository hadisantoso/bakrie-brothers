@BBApp.module "BackupApp", (BackupApp, App, Backbone, Marionette, $, _) ->
  
  class BackupApp.Router extends Marionette.AppRouter
    appRoutes:
      "admin/backups" : "list"
  
  API =
    list: ->
      if !App.validUser()
        App.goToLogin()
        return
      App.setupAdminUser()
      new BackupApp.List.Controller
        region: App.mainRegion
        
  App.addInitializer ->
    new BackupApp.Router
      controller: API