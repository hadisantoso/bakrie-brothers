@BBApp.module "PostApp", (PostApp, App, Backbone, Marionette, $, _) ->
	
	class PostApp.Router extends Marionette.AppRouter
		appRoutes:
			"admin/post/:id/edit"	: "edit"
			"admin/post/:type"		: "list"
			"admin/post"					: "list"

	API =
		list: (type) ->			
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			new PostApp.List.Controller(type)
		
		newPost: (type, region) ->
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			new PostApp.New.Controller
				type: type
				region: region
		
		edit: (id, member) ->
			if !App.validUser()
				App.goToLogin()
				return
			App.setupAdminUser()
			new PostApp.Edit.Controller
				id: id
				post: member
	
	App.commands.setHandler "new:post:member", (type, region) ->
		API.newPost type, region
	
	App.vent.on "post:member:clicked post:created", (member) ->
		API.edit member.id, member
	
	App.vent.on "post:cancelled post:updated", (post) ->
		console.log post
		if post.get("_type")
			type = post.get("_type")
			App.navigate(Routes.admin_post_index_path() + "/#{type}", {trigger: true})
		else
			App.navigate Routes.admin_post_index_path()
	
	App.addInitializer ->
		new PostApp.Router
			controller: API