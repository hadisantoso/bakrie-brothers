@BBApp.module "PostApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->
	
	class Edit.Controller extends App.Controllers.Base
		
		initialize: (options) ->

			@images = App.request "admin:post:entities", "ImagePost"
			@listenTo @images, "sync:stop", =>

				post = options.post
				id = options.id

				post or= App.request "admin:post:entity", id

				@listenTo post, "updated", ->
					App.vent.trigger "post:updated", post

				@listenTo post, "upload:success", ->
					console.log "post upload succeeded"
				
				App.execute "when:fetched", post, =>
					@layout = @getLayoutView post
					
					@listenTo @layout, "show", =>
						@titleRegion post
						@formRegion post
				
					@show @layout

		
		titleRegion: (post) ->
			titleView = @getTitleView post
			@layout.titleRegion.show titleView
		
		formRegion: (post) ->
			editView = @getEditView post
			
			@listenTo editView, "form:cancel", ->
				App.vent.trigger "post:cancelled", post
			
			formView = App.request "form:wrapper", editView, 
				imageUpload: true
			
			@layout.formRegion.show formView
		
		getTitleView: (post) ->
			new Edit.Title
				model: post
		
		getLayoutView: (post) ->
			new Edit.Layout
				model: post
		
		getEditView: (post) ->
			new Edit.Post
				model: post
				images: @images