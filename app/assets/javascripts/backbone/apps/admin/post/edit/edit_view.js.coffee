@BBApp.module "PostApp.Edit", (Edit, App, Backbone, Marionette, $, _) ->
	
	class Edit.Layout extends App.Views.Layout
		template: "admin/post/edit/edit_layout"
		
		regions:
			titleRegion:	"#title-region"
			formRegion: 	"#form-region"
	
	class Edit.Title extends App.Views.ItemView
		template: "admin/post/edit/edit_title"
		
		modelEvents:
			"updated" : "render"
	
	class Edit.Post extends App.Views.ItemView
		template: "admin/post/edit/edit_post"

		events:
			"click .btn_insert_into_content": (e) ->
				e.preventDefault()
				val = "\nimage=" + $("#ddl_images :selected").val() + "\n"
				@insertAtCaret("markdown", val)

			"click .btn_insert_into_content2": (e) ->
				e.preventDefault()
				val = "\nimage=" + $("#ddl_images2 :selected").val() + "\n"
				@insertAtCaret("markdown2", val)

		modelEvents:
			"upload:success" : -> 
				@$(".document_preview").attr("href", @model.get("document").document_url)

			"upload2:success" : -> 
				@$(".document_preview2").attr("href", @model.get("document2").document2_url)

			"upload:header_image:success" : -> 
				@$(".header_image_preview").attr("src", @model.get("header_image").thumbnail_resolution)

			"upload:image:success" : -> 
				@$(".image_preview").attr("src", @model.get("image").thumbnail_resolution)

			"upload:image2:success" : -> 
				@$(".image2_preview").attr("src", @model.get("image2").thumbnail_resolution)

			"upload:image3:success" : -> 
				@$(".image3_preview").attr("src", @model.get("image3").thumbnail_resolution)

			"upload:image4:success" : -> 
				@$(".image4_preview").attr("src", @model.get("image4").thumbnail_resolution)

			"remove:image:success" : -> 
				@$(".image_preview").attr("src", "http://placehold.it/100x100")

			"remove:image2:success" : -> 
				@$(".image2_preview").attr("src", "http://placehold.it/100x100")

			"remove:image3:success" : -> 
				@$(".image3_preview").attr("src", "http://placehold.it/100x100")

			"remove:image4:success" : -> 
				@$(".image4_preview").attr("src", "http://placehold.it/100x100")

		templateHelpers: ->
			_.extend {}, Edit.Post::templateHelpers,

			showDocumentUpload: =>
				App.showDocumentUpload(@model.get("_type"))

			showPagePostUpload: =>
				App.showPagePostUpload(@model.get("_type"))

			showImageUpload: =>
				App.showImageUpload(@model.get("_type"))

			showHeaderImageUpload: =>
				App.showHeaderImageUpload(@model.get("_type"))

			showImageInsertion: =>
				App.showImageInsertion(@model.get("_type"))

			showHumanReadableKey: =>
				App.showHumanReadableKey(@model.get("_type"))

			showAdditionalImageUploads: =>
				App.showAdditionalImageUploads(@model.get("_type"))				

			showLanguageSelection: =>
				App.showLanguageSelection(@model.get("_type"))

			showAuthor: =>
				App.showAuthor(@model.get("_type"))

			images: =>
				@options.images.models


		insertAtCaret: (areaId, text) ->
			txtarea = document.getElementById(areaId)
			scrollPos = txtarea.scrollTop
			strPos = 0
			br = if txtarea.selectionStart or txtarea.selectionStart == '0' then 'ff' else if document.selection then 'ie' else false
			if br == 'ie'
				txtarea.focus()
				range = document.selection.createRange()
				range.moveStart 'character', -txtarea.value.length
				strPos = range.text.length
			else if br == 'ff'
			  strPos = txtarea.selectionStart
				front = txtarea.value.substring(0, strPos)
				back = txtarea.value.substring(strPos, txtarea.value.length)
				txtarea.value = front + text + back
				strPos = strPos + text.length
			if br == 'ie'
			  txtarea.focus()
			  range = document.selection.createRange()
			  range.moveStart 'character', -txtarea.value.length
			  range.moveStart 'character', strPos
			  range.moveEnd 'character', 0
			  range.select()
			else if br == 'ff'
			  txtarea.selectionStart = strPos
			  txtarea.selectionEnd = strPos
			  txtarea.focus()
				txtarea.scrollTop = scrollPos
			return

			# fileSizeBox = @$(".form_file_size")
			# progressBox = @$(".form_progress")
			# uploadButton = $(".uploadButton")
			# uploader = new (ss.SimpleUpload)(
			#   button: uploadButton
			#   url: "/admin/post/#{@model.id}/upload_image"
			#   name: 'image_upload'
			#   responseType: 'json'
			#   allowedExtensions: [
			#     'jpg'
			#     'jpeg'
			#     'png'
			#     'gif'
			#   ]
			#   maxSize: 1024
			#   onSubmit: (filename, extension) ->
			#     @setFileSizeBox(fileSizeBox)
			#     # designate this element as file size container
			#     @setProgressBar(progressBox)
			#     # designate as progress bar
			#     return
			#   onComplete: (filename, response) ->
			#     if !response
			#       alert filename + 'upload failed'
			#       return false
			#     # do something with response...
			#     return
			# )
