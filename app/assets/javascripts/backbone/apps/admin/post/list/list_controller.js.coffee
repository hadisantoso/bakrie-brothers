@BBApp.module "PostApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Controller extends App.Controllers.Base
		
		initialize: (type) ->
			@type = type
			@page = 1
			List.type = type
			#@collection = App.request "admin:post:entities", (type)
			@collection = App.request "admin:post:get", type, "", "", @page

			App.execute "when:fetched", @collection, =>
			
				@layout = @getLayoutView @collection
				
				@listenTo @layout, "close", @close
			
				@listenTo @layout, "show", =>
					@titleRegion()
					@panelRegion()
					@postRegion @collection
			
				@show @layout

			App.vent.unbind "post:more"
			App.vent.on "post:more", =>
				console.log "more"
				@page = @page + 1
				if @collection != null && @collection.length > 0
					@collection = App.request "admin:post:get:more", @collection, @type, @page
					#@collection = App.request "admin:post:get:more", (type)
		
		titleRegion: ->
			titleView = @getTitleView()
			@layout.titleRegion.show titleView
		
		panelRegion: ->
			panelView = @getPanelView()
			
			@listenTo panelView, "new:post:button:clicked", =>
				@newRegion(@type)
			
			@layout.panelRegion.show panelView
		
		newRegion: (type) ->
			App.execute "new:post:member", type, @layout.postRegion
		
		postRegion: (post) ->
			postView = @getPostView post
			
			@listenTo postView, "childview:post:member:clicked", (child, args) ->
				App.vent.trigger "post:member:clicked", args.model
			
			@listenTo postView, "childview:post:delete:clicked", (child, args) ->
				model = args.model
				if confirm "Are you sure you want to delete #{model.get("title")}?"
					model.save({"status":"deleted"})
				else 
					false

			@listenTo postView, "childview:post:publish:toggle:clicked", (child, args) ->
				model = args.model
				if model.get("status") == "published"
					model.save({"status":"unpublished"})
				else if model.get("status") == "unpublished"
					model.save({"status":"published"})
						
			@layout.postRegion.show postView
		
		getPostView: (post) ->
			new List.Post
				collection: post
		
		getPanelView: ->
			new List.Panel
		
		getTitleView: ->
			new List.Title
		
		getLayoutView: (post) ->
			new List.Layout
				collection: post