@BBApp.module "PostApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Layout extends App.Views.Layout
		template: "admin/post/list/list_layout"

		regions:
			titleRegion: 	"#title-region"
			panelRegion:	"#panel-region"
			newRegion:		"#new-region"
			postRegion:		"#post-region"
	
	class List.Title extends App.Views.ItemView
		template: "admin/post/list/_title"

		templateHelpers: ->
			_.extend {}, List.Title::templateHelpers,
			type: ->
				camelCased = List.type.replace(/_([a-zA-Z])/g, 
					(g) ->
				  	return g[1].toUpperCase()
				 )
				return camelCased[0].toUpperCase() + camelCased.substring(1,camelCased.length)

	class List.Panel extends App.Views.ItemView
		template: "admin/post/list/_panel"
		
		triggers:
			"click #new-post" : "new:post:button:clicked"
	
	class List.PostMember extends App.Views.ItemView
		template: "admin/post/list/_post_member"
		tagName: "tr"
		className: "post-member"
			
		triggers:
			"click .btn_delete"         : "post:delete:clicked"
			"click .btn_publish" 				: "post:publish:toggle:clicked"
			"click .btn_edit" 					: "post:member:clicked"

		modelEvents:
			"updated" : "render"

		templateHelpers: ->
			_.extend {}, List.PostMember::templateHelpers,

			type: ->
				camelCased = List.type.replace(/_([a-zA-Z])/g, 
					(g) ->
				  	return g[1].toUpperCase()
				 )
				return camelCased[0].toUpperCase() + camelCased.substring(1,camelCased.length)

			showDocumentUpload: =>
				App.showDocumentUpload(@model.get("_type"))

			showImageUpload: =>
				App.showImageUpload(@model.get("_type"))

			showLanguageSelection: =>
				App.showLanguageSelection(@model.get("_type"))

			showPublishingAction: =>
				App.showPublishingAction(@model.get("_type"))

			showAuthor: =>
				App.showAuthor(@model.get("_type"))

			showHumanReadableKey: =>
				App.showHumanReadableKey(@model.get("_type"))

	
	class List.Empty extends App.Views.ItemView
		template: "admin/post/list/_empty"
		tagName: "tr"
	
	class List.Post extends App.Views.CompositeView
		template: "admin/post/list/_post"
		childView: List.PostMember
		emptyView: List.Empty
		childViewContainer: "tbody"
 
		templateHelpers: ->
			_.extend {}, List.Post::templateHelpers,

			showDocumentUpload: =>
				App.showDocumentUpload(List.type)

			showImageUpload: =>
				App.showImageUpload(List.type)				

			showLanguageSelection: =>
				App.showLanguageSelection(List.type)

			showPublishingAction: =>
				App.showPublishingAction(List.type)

			showAuthor: =>
				App.showAuthor(List.type)

			showHumanReadableKey: =>
				App.showHumanReadableKey(List.type)

		collectionEvents:
			"sync:stop": ->
				if @collection.length == 0
					@$(".paging").hide()
				else if @collection.length == @currentLength
					@$(".paging").hide()

		events:
			"click .btn_more": (e) ->
				e.preventDefault()
				@currentLength = @collection.length
				App.vent.trigger "post:more"