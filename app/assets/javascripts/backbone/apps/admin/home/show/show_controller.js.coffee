@BBApp.module "Admin.HomeApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log "initializing HomeApp.Show.Controller"
      @layout = @getLayoutView()

      @listenTo @layout, "show", =>
        App.module("Admin.HeaderApp").start(
          region: App.headerRegion
        )
        App.module("Admin.FooterApp").start(
          region: App.footerRegion
        )
      @region.show @layout

    getLayoutView: ->
      new Show.Layout()