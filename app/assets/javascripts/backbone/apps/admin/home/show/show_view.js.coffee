@BBApp.module "Admin.HomeApp.Show", (Show, App, Backbone, Marionette, $, _) ->
  
  class Show.Layout extends App.Views.Layout
    template: "admin/home/show/layout"

    regions:
      headerRegion:  "#header-region"
      footerRegion:  "#footer-region"