@BBApp.module "Admin.HomeApp", (HomeApp, App, Backbone, Marionette, $, _) ->
  @startWithParent = false
  
  class HomeApp.Router extends Marionette.AppRouter
    appRoutes:
      "admin" : "start"
      "admin/home" : "start"
      

  API =
    start: ->
      if !App.validUser()
        App.goToLogin()
        return
      App.setupAdminUser()
      console.log "starting admin home app"
      new HomeApp.Show.Controller
        region: App.mainRegion

  App.addInitializer ->
    console.log "admin homeapp addInitializer"
    new HomeApp.Router
      controller: API