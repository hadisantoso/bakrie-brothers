@BBApp.module "Admin.HeaderApp.List", (List, App, Backbone, Marionette, $, _) ->
	
	class List.Header extends App.Views.ItemView
    template: "admin/header/list/header"
    events:
      "click .btnLogout":"logout"

    logout: ->
      App.logout()
      App.navigate "/login"

		

    