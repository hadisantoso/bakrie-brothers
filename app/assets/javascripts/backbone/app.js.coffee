@BBApp = do (Backbone, Marionette) ->
  App = new Marionette.Application

  App.on "initialize:before", (options) ->
    App.environment = options.environment
  
  App.addRegions
    headerRegion: "#header-region"
    mainRegion:   "#main-region"
    footerRegion: "#footer-region"
  
  App.rootRoute = "" #routed by home_app
  
  App.addInitializer ->
    #App.module("HeaderApp").start()
    #App.module("FooterApp").start()

  App.reqres.setHandler "default:region", ->
    App.mainRegion


  #access
  App.showHumanReadableKey = (type) ->
    return s.include("PagePost page_post", type)

  App.showHeaderImageUpload = (type) ->
    return s.include("PagePost page_post", type)

  App.showImageUpload = (type) ->
    return s.include("NewsPost news_post PrPost pr_post CommissionerPost commissioner_post DirectorPost director_post CorporateSecretaryPost corporate_secretary_post PagePost page_post HomeImagePost home_image_post CsrPost csr_post HsePost hse_post", type)

  App.showImageInsertion = (type) ->
    return s.include("PagePost page_post", type)

  App.showAdditionalImageUploads = (type) ->
    return s.include("NewsPost news_post PrPost pr_post PagePost page_post CsrPost csr_post HsePost hse_post", type)

  App.showDocumentUpload = (type) ->
    return s.include("FinancialReportPost financial_report_post AnnualReportPost annual_report_post CompanyPresentationPost company_presentation_post RupsPost rups_post PublicExposePost public_expose_post CorrespondencePost correspondence_post AnnouncementDocumentPost announcement_document_post CorporateGovernanceDocumentPost corporate_governance_document_post", type)

  App.showPagePostUpload = (type) ->
    return s.include("PagePost page_post", type)

  App.showLanguageSelection = (type) ->
    return s.include("PagePost page_post", type)

  App.showPublishingAction = (type) ->
    return !s.include("PagePost page_post", type)

  App.showAuthor = (type) ->
    return s.include("NewsPost news_post PrPost pr_post", type)

  App.setupEndUser = ->
    App.startModule("EndUser.HeaderApp", App.headerRegion)
    App.startModule("EndUser.FooterApp", App.footerRegion)

  App.setupAdminUser = ->
    App.startModule("Admin.HeaderApp", App.headerRegion)
    App.startModule("Admin.FooterApp", App.footerRegion)

  App.loggedIn = false
  App.loggedInNickname = null
  App.loggedInDepartment = null
  App.loggedInPostAccess = null
  App.token = null
  App.language = if Cookies.get("language") == undefined then "id" else Cookies.get("language")

  App.logout = ->
    Cookies.remove('bakrie-brothersLoggedIn')
    Cookies.remove('bakrie-brothersNickname')
    Cookies.remove('bakrie-brothersToken')
    Cookies.remove('bakrie-brothersDepartment')
    Cookies.remove('bakrie-brothersPostAccess')
    Cookies.remove('bakrie-brothersPageAccess')

  App.goToLogin = ->
    @headerRegion.empty()
    @footerRegion.empty()
    @navigate "/login"

  App.validUser = ->
    loggedIn = Cookies.get('bakrie-brothersLoggedIn') == "true"

    if loggedIn
      App.loggedInDepartment = Cookies.get("bakrie-brothersDepartment")
      App.loggedInNickname = Cookies.get("bakrie-brothersNickname")
      App.loggedInPostAccess = Cookies.get("bakrie-brothersPostAccess").split(',')
      App.loggedInPageAccess = Cookies.get("bakrie-brothersPageAccess").split(',')
      App.token = Cookies.get("bakrie-brothersToken")

    return loggedIn
    
  App.startModule = (moduleName, region) ->
    App.module(moduleName).stop()
    App.module(moduleName).start(region: region)

  App.scrollToTop = ->
    window.scrollTo(0, 0)


  App.vent.on "login:successful", (auth) ->
    Cookies.set('bakrie-brothersLoggedIn', "true" , { expires: 1 });
    Cookies.set('bakrie-brothersNickname', auth.get("nickname") , { expires: 1 });
    Cookies.set('bakrie-brothersToken', auth.get("token") , { expires: 1 });
    Cookies.set('bakrie-brothersDepartment', auth.get("department") , { expires: 1 });
    Cookies.set('bakrie-brothersPostAccess', auth.get("post_access").join(',') , { expires: 1 });
    Cookies.set('bakrie-brothersPageAccess', auth.get("page_access").join(',') , { expires: 1 });
    App.loggedIn = true    
    App.navigate "/admin/home"

  App.vent.on "login:failed", (auth) ->
    Cookies.set('loggedIn', "false" , { expires: 1 });

  App.on "start", (options) ->
    @startHistory()    
    
    currentRoute = @getCurrentRoute()

    @navigate(@rootRoute, trigger: true) unless @getCurrentRoute()
    #App.csrfToken = $("meta[name='csrf-token']").attr('content');

  App