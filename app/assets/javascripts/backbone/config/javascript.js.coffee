Array::insertAt = (index, item) ->
	@splice(index, 0, item)
	@

String::capitalize = ->
  return this.replace(/(?:^|\s)\S/g,
    (a) ->
      return a.toUpperCase();
  )

console = {}
console.log = ->
window.console = console
