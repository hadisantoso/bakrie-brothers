@BBApp.module "Components.TimelineList", (TimelineList, App, Backbone, Marionette, $, _) ->

  title = ""
  
  class TimelineList.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log options
      TimelineList.title = options.title
      @post_type = options.post_type
      @collection = null
      console.log "initializing Components.TimelineList"
      layout = @getLayout()
      view = @getView()
      region = options.region
      region.show layout
      layout.region.show view

    getLayout: ->
      new TimelineList.Layout()

    getView: =>
      @collection = App.request "end_user:post:get", @post_type, "", ""
      view = new TimelineList.Posts
        collection: @collection

      return view      


  App.reqres.setHandler "timeline:list", (title, post_type, region) ->
    controller = new TimelineList.Controller
      title: title
      post_type: post_type
      region: region