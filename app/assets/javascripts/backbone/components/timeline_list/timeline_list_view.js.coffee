@BBApp.module "Components.TimelineList", (TimelineList, App, Backbone, Marionette, $, _) ->
  
  class TimelineList.Layout extends App.Views.Layout
    template: "timeline_list/layout"

    templateHelpers: ->
      title: ->
        TimelineList.title

    regions:
      region: "#region"

  class TimelineList.Post extends App.Views.ItemView
    template: "timeline_list/post"
    className: "cd-timeline-block"  

  class TimelineList.Posts extends App.Views.CompositeView
    template: "timeline_list/posts"    
    childView: TimelineList.Post
    childViewContainer: ".posts"

    collectionEvents:
      "sync:stop": ->
        console.log "old: #{@currentLength}"
        console.log "collection size is #{@collection.length}"
        console.log @collection
        if @collection.length == 0
          @$(".paging").hide()
        else if @collection.length == @currentLength
          @$(".paging").hide()

    events:
      "click .btn_more": (e) ->
        e.preventDefault()
        @currentLength = @collection.length

        App.vent.trigger "post:more"