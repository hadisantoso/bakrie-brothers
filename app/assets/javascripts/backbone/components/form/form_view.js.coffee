@BBApp.module "Components.Form", (Form, App, Backbone, Marionette, $, _) ->
	
	class Form.FormWrapper extends App.Views.Layout
		template: "form/form"
		
		tagName: "form"
		attributes: ->
			"data-type": @getFormDataType()
		
		regions:
			formContentRegion: "#form-content-region"
		
		ui:
			buttonContainer: "ul.inline-list"
		
		triggers:
			"submit"																	   : "form:submit"
			"click [data-form-button='cancel']"				   : "form:cancel"

			"click [data-form-button='header_image_upload']" 	 : "form:header_image_upload"

			"click [data-form-button='image_upload']" 	 : "form:image_upload"
			"click [data-form-button='image2_upload']" 	 : "form:image2_upload"
			"click [data-form-button='image3_upload']" 	 : "form:image3_upload"
			"click [data-form-button='image4_upload']" 	 : "form:image4_upload"

			"click [data-form-button='remove_header_image']" 	 : "form:remove_header_image"
			"click [data-form-button='remove_image']" 	 : "form:remove_image"
			"click [data-form-button='remove_image2']" 	 : "form:remove_image2"
			"click [data-form-button='remove_image3']" 	 : "form:remove_image3"
			"click [data-form-button='remove_image4']" 	 : "form:remove_image4"


			"click [data-form-button='document_upload']" : "form:document_upload"
			"click [data-form-button='document_upload2']" : "form:document_upload2"
		
		modelEvents:
			"change:_errors" 	: "changeErrors"
			"sync:start"			:	"syncStart"
			"sync:stop"				:	"syncStop"
		
		initialize: ->
			@setInstancePropertiesFor "config", "buttons"
		
		serializeData: ->
			imageUpload: @config.imageUpload
			footer: @config.footer
			buttons: @buttons?.toJSON() ? false
		
		onShow: ->
			_.defer =>
				@focusFirstInput() if @config.focusFirstInput
				@buttonPlacement() if @buttons
		
		buttonPlacement: ->
			@ui.buttonContainer.addClass @buttons.placement
		
		focusFirstInput: ->
			@$(":input:visible:enabled:first").focus()
		
		getFormDataType: ->
			if @model.isNew() then "new" else "edit"
		
		changeErrors: (model, errors, options) ->
			if @config.errors
				if _.isEmpty(errors) then @removeErrors() else @addErrors errors
		
		removeErrors: ->
			@$(".error").removeClass("error").find("small").remove()
		
		addErrors: (errors = {}) ->
			for name, array of errors
				@addError name, array[0]
		
		addError: (name, error) ->
			el = @$("[name='#{name}']")
			sm = $("<small>").text(error)
			el.after(sm).closest(".row").addClass("error")
		
		syncStart: (model) ->
			@addOpacityWrapper() if @config.syncing
		
		syncStop: (model) ->
			@addOpacityWrapper(false) if @config.syncing