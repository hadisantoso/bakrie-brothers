@BBApp.module "Components.Form", (Form, App, Backbone, Marionette, $, _) ->
	
	class Form.Controller extends App.Controllers.Base
		
		initialize: (options = {}) ->
			@contentView = options.view
			
			@formLayout = @getFormLayout options.config
			
			@listenTo @formLayout, "show", @formContentRegion
			@listenTo @formLayout, "form:submit", @formSubmit
			@listenTo @formLayout, "form:cancel", @formCancel

			@listenTo @formLayout, "form:header_image_upload", @formHeaderImageUpload

			@listenTo @formLayout, "form:image_upload", @formImageUpload
			@listenTo @formLayout, "form:image2_upload", @formImageUpload2
			@listenTo @formLayout, "form:image3_upload", @formImageUpload3
			@listenTo @formLayout, "form:image4_upload", @formImageUpload4

			@listenTo @formLayout, "form:remove_header_image", @formRemoveHeaderImage

			@listenTo @formLayout, "form:remove_image", @formRemoveImage
			@listenTo @formLayout, "form:remove_image2", @formRemoveImage2
			@listenTo @formLayout, "form:remove_image3", @formRemoveImage3
			@listenTo @formLayout, "form:remove_image4", @formRemoveImage4

			@listenTo @formLayout, "form:document_upload", @formDocumentUpload
			@listenTo @formLayout, "form:document_upload2", @formDocumentUpload2
		
		formCancel: ->
			@contentView.triggerMethod "form:cancel"
		
		formSubmit: ->
			data = Backbone.Syphon.serialize @formLayout
			if @contentView.triggerMethod("form:submit", data) isnt false
				model = @contentView.model
				collection = @contentView.collection
				@processFormSubmit data, model, collection
		
		formHeaderImageUpload: ->
			@imageUpload("header_image")

		formImageUpload: ->
			@imageUpload("image")

		formImageUpload2: ->
			@imageUpload("image2")

		formImageUpload3: ->
			@imageUpload("image3")

		formImageUpload4: ->
			@imageUpload("image4")

		imageUpload: (image) ->
			@formLayout.addOpacityWrapper()
			file = $(@formLayout.el).find("input#" + image + "_upload[type='file']")[0].files[0]

			@formLayout.model.uploadImage(file, image, "upload:#{image}:success",
				=>
			    @formLayout.addOpacityWrapper(false)
			)			


		formRemoveHeaderImage: ->
			@removeImage("header_image")

		formRemoveImage: ->
			@removeImage("image")

		formRemoveImage2: ->
			@removeImage("image2")

		formRemoveImage3: ->
			@removeImage("image3")

		formRemoveImage4: ->
			@removeImage("image4")

		removeImage: (image) ->
			@formLayout.addOpacityWrapper()
			@formLayout.model.removeImage(image,
				=>
			    @formLayout.addOpacityWrapper(false)
			    @trigger "remove:#{image}:success"
			)			

		formDocumentUpload: ->
			@formLayout.addOpacityWrapper()
			file = $(@formLayout.el).find("input#document_upload[type='file']")[0].files[0]

			@formLayout.model.uploadDocument(file, "upload:success", 
				=>
			    @formLayout.addOpacityWrapper(false)
			)

		formDocumentUpload2: ->
			@formLayout.addOpacityWrapper()
			file = $(@formLayout.el).find("input#document_upload2[type='file']")[0].files[0]

			@formLayout.model.uploadDocument2(file, "upload2:success",
				=>
			    @formLayout.addOpacityWrapper(false)
			)

		processFormSubmit: (data, model, collection) ->
			model.save data,
				collection: collection
		
		onClose: ->
			console.log "onClose", @
		
		formContentRegion: ->
			@region = @formLayout.formContentRegion
			@show @contentView
		
		getFormLayout: (options = {}) ->
			config = @getDefaultConfig _.result(@contentView, "form")
			_.extend config, options

			buttons = @getButtons config.buttons

			new Form.FormWrapper
				config: config
				model: @contentView.model
				buttons: buttons
		
		getDefaultConfig: (config = {}) ->
			_.defaults config,
				footer: true
				focusFirstInput: true
				errors: true
				syncing: true
				imageUpload: false
				documentUpload: false
		
		getButtons: (buttons = {}) ->
			App.request("form:button:entities", buttons, @contentView.model) unless buttons is false
	
	App.reqres.setHandler "form:wrapper", (contentView, options = {}) ->
		throw new Error "No model found inside of form's contentView" unless contentView.model
		formController = new Form.Controller
			view: contentView
			config: options
		formController.formLayout