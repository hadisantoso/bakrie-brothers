@BBApp.module "Components.AccordionList", (AccordionList, App, Backbone, Marionette, $, _) ->

  title = ""
  
  class AccordionList.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log options
      AccordionList.title = options.title
      @post_type = options.post_type
      @collection = null
      @page = 1
      console.log "initializing Components.AccordionList"
      layout = @getLayout()
      view = @getView()
      region = options.region
      region.show layout
      layout.region.show view

    getLayout: ->
      new AccordionList.Layout()

    getView: =>
      @collection = App.request "end_user:post:get", @post_type, "", "", @page
      view = new AccordionList.Posts
        collection: @collection

      return view      


  App.reqres.setHandler "accordion:list", (title, post_type, region) ->
    controller = new AccordionList.Controller
      title: title
      post_type: post_type
      region: region