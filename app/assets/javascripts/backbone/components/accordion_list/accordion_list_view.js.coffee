@BBApp.module "Components.AccordionList", (AccordionList, App, Backbone, Marionette, $, _) ->
  
  class AccordionList.Layout extends App.Views.Layout
    template: "accordion_list/layout"

    templateHelpers: ->
      title: ->
        AccordionList.title

    regions:
      region: "#region"

  class AccordionList.Post extends App.Views.ItemView
    template: "accordion_list/post"    

  class AccordionList.Posts extends App.Views.CompositeView
    template: "accordion_list/posts"    
    childView: AccordionList.Post
    childViewContainer: ".posts"

    collectionEvents:
      "sync:stop": ->
        console.log "old: #{@currentLength}"
        console.log "collection size is #{@collection.length}"
        console.log @collection
        if @collection.length == 0
          @$(".paging").hide()
        else if @collection.length == @currentLength
          @$(".paging").hide()

    events:
      "click .btn_more": (e) ->
        e.preventDefault()
        @currentLength = @collection.length

        App.vent.trigger "post:more"