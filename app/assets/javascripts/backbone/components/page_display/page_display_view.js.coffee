@BBApp.module "Components.PageDisplay", (PageDisplay, App, Backbone, Marionette, $, _) ->

  class PageDisplay.Page extends App.Views.ItemView
    template: "page_display/page"

  class PageDisplay.Pages extends App.Views.CompositeView
    template: "page_display/pages"
    childView: PageDisplay.Page
    childViewContainer: ".pages"

  class PageDisplay.NoHeaderPage extends App.Views.ItemView
    template: "page_display/noheaderpage"

  class PageDisplay.NoHeaderPages extends App.Views.CompositeView
    template: "page_display/pages"
    childView: PageDisplay.NoHeaderPage
    childViewContainer: ".pages"