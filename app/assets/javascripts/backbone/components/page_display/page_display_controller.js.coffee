@BBApp.module "Components.PageDisplay", (PageDisplay, App, Backbone, Marionette, $, _) ->

  title = ""
  
  class PageDisplay.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log options
      PageDisplay.title = options.title
      @collection = null
      view = @getView(options.title, options.hideHeader)
      region = options.region
      region.show view
      @cb = options.cb

    getView: (title, hideHeader) =>
      @collection = App.request "end_user:post:page:get", title
      
      @listenTo @collection, "reset", ->
        if @cb && @collection.models && @collection.models.length > 0
          @cb(@collection.models[0])

      if hideHeader
        view = new PageDisplay.NoHeaderPages
          collection: @collection        
      else
        view = new PageDisplay.Pages
          collection: @collection        

      return view      


  App.reqres.setHandler "page:display", (title, region, cb, hideHeader=false) ->
    controller = new PageDisplay.Controller
      title: title
      region: region
      cb: cb
      hideHeader: hideHeader