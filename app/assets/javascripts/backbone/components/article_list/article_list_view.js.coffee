@BBApp.module "Components.ArticleList", (ArticleList, App, Backbone, Marionette, $, _) ->
  
  class ArticleList.Layout extends App.Views.Layout
    template: "article_list/layout"

    templateHelpers: ->
      title: ->
        ArticleList.title

    regions:
      region: "#region"
      contentRegion: "#content-region"

  class ArticleList.Post extends App.Views.ItemView
    template: "article_list/post"    

  class ArticleList.Posts extends App.Views.CompositeView
    template: "article_list/posts"    
    childView: ArticleList.Post
    childViewContainer: ".posts"

    collectionEvents:
      "sync:stop": ->
        console.log "old: #{@currentLength}"
        console.log "collection size is #{@collection.length}"
        console.log @collection
        if @collection.length == 0
          @$(".paging").hide()
        else if @collection.length == @currentLength
          @$(".paging").hide()

    events:
      "click .btn_more": (e) ->
        e.preventDefault()
        @currentLength = @collection.length

        App.vent.trigger "post:more"