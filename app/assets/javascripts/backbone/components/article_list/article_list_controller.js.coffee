@BBApp.module "Components.ArticleList", (ArticleList, App, Backbone, Marionette, $, _) ->

  title = ""
  
  class ArticleList.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      console.log options
      ArticleList.title = options.title
      @post_type = options.post_type
      @collection = null
      @page = 1
      console.log "initializing Components.ArticleList"
      @layout = @getLayout()
      view = @getView()
      region = options.region
      region.show @layout
      @layout.region.show view
      postpage = options.page

      if postpage != null
        App.request "page:display", postpage, @layout.contentRegion, @cb, hideHeader=true

      #handle venting of pagination's "more"
      App.vent.unbind "post:more"
      App.vent.on "post:more", =>
        console.log "more"
        @page = @page + 1
        if @collection != null && @collection.length > 0
          @collection = App.request "end_user:post:get:more", @collection, @post_type, @page, null, null, true

    getLayout: ->
      new ArticleList.Layout()

    getView: =>
      @collection = App.request "end_user:post:get", @post_type, "", "", @page, true
      view = new ArticleList.Posts
        collection: @collection

      return view      

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

  App.reqres.setHandler "article:list", (title, page, post_type, region) ->
    controller = new ArticleList.Controller
      title: title
      page: page
      post_type: post_type
      region: region