@BBApp.module "Components.DocumentTable", (DocumentTable, App, Backbone, Marionette, $, _) ->

  class DocumentTable.Controller extends App.Controllers.Base
    
    initialize: (options) ->
      @title = options.title
      @post_type = options.post_type
      @collection = null
      page = options.page

      @layout = @getLayout()
      
      region = options.region
      region.show @layout

      @layout.documentsRegion.show @getTableView()
      @layout.yearsRegion.show @getYearsView()      
      if page != null
        App.request "page:display", page, @layout.contentRegion, @cb
      @setupYearsNavigation()

    getLayout: ->
      new DocumentTable.Layout()

    getTableView: =>
      @collection = App.request "end_user:post:get", @post_type, "view all", "post_date"
      view = new DocumentTable.Posts
        collection: @collection

      return view      

    getYearsView: ->
      years = App.request "year:get", 2011, 2017, true
      yearsView = new DocumentTable.Years
        collection: years
      
      return yearsView

    setupYearsNavigation: ->
      App.vent.unbind "year:click"
      App.vent.on "year:click", (year) =>
        if year == null || year == undefined
          collection = App.request "end_user:post:get", @post_type
        else
          collection = App.request "end_user:post:get", @post_type, year, "post_date"

        view = new DocumentTable.Posts
          collection: collection

        @layout.documentsRegion.show view

    cb: (pagePost) =>
      @layout.showHeader(pagePost.get("header_image").original_resolution)

  App.reqres.setHandler "document:table", (title, page, post_type, region) ->
    controller = new DocumentTable.Controller
      title: title
      page: page
      post_type: post_type
      region: region