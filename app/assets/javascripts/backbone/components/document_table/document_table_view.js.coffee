@BBApp.module "Components.DocumentTable", (DocumentTable, App, Backbone, Marionette, $, _) ->
  
  class DocumentTable.Layout extends App.Views.Layout
    template: "document_table/layout"

    regions:
      contentRegion: "#content-region"
      documentsRegion: "#documents-region"
      yearsRegion: "#years-region"

  class DocumentTable.Post extends App.Views.ItemView
    template: "document_table/document"    
    tagName: "tr"

    onShow: ->
      $('.fancybox-media').fancybox(
        openEffect  : 'none'
        closeEffect : 'none'
        helpers : 
          media : {}        
      )

  class DocumentTable.Posts extends App.Views.CompositeView
    template: "document_table/documents"
    childView: DocumentTable.Post
    childViewContainer: "tbody"

  class DocumentTable.Year extends App.Views.ItemView
    template: "document_table/year"
    tagName: "li"

    events: 
      "click .year": (e) ->
        e.preventDefault()
        App.vent.trigger "year:click", @$(e.target).attr("data-value")

  class DocumentTable.Years extends App.Views.CompositeView
    template: "document_table/years"
    childView: DocumentTable.Year
    childViewContainer: ".years"