@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.Post extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_post_index_path()
  
    uploadDocument: (file, successTriggerEvent, cb) ->
      @uploadFile(file, "/admin/post/#{@id}/upload_document", successTriggerEvent, cb, ->)

    uploadDocument2: (file, successTriggerEvent, cb) ->
      @uploadFile(file, "/admin/post/#{@id}/upload_document2", successTriggerEvent, cb, ->)

    uploadImage: (file, target, successTriggerEvent, cb) ->
      @uploadFile(file, "/admin/post/#{@id}/upload_#{target}", successTriggerEvent, cb, ->)    

    removeImage: (image, cb) ->
      @save({"remove_image": image})
      @listenTo @, "updated", =>
        cb()

  class Entities.PostCollection extends App.Entities.Collection
    model: Entities.Post
    
    url: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_post_index_path()
  
  API =
    getPost: (type) ->
      post = new Entities.PostCollection
      post.fetch
        data:
          type: type
          token: App.token
        reset: true
      post
    
    getPostMember: (id) ->
      member = new Entities.Post
        id: id
      member.fetch(
        data:
          token: App.token
      )
      member
    
    newPostMember: (type) ->
      post = new Entities.Post
      post.set("_type", type)
      return post
  

    #queries
    getPosts: (type, filter, filter_type, page) ->
      post = new Entities.PostCollection
      post.fetch
        data:
          type: type
          token: App.token
          filter: filter
          filter_type: filter_type
          page: page
        reset: true
      post

    getMorePosts: (post, type, page, filter, filter_type) ->
      post.fetch
        data:
          type: type
          token: App.token
          filter: filter
          filter_type: filter_type
          page: page
        remove: false
      post

  App.reqres.setHandler "admin:post:entities", (type) ->
    API.getPost(type)
  
  App.reqres.setHandler "admin:post:entity", (id) ->
    API.getPostMember id
  
  App.reqres.setHandler "admin:new:post:entity", (type) ->
    API.newPostMember(type)

  App.reqres.setHandler "admin:post:get", (type, filter, filter_type, page) ->
    API.getPosts(type, filter, filter_type, page)

  App.reqres.setHandler "admin:post:get:more", (post, type, page, filter, filter_type) ->
    API.getMorePosts(post, type, page, filter, filter_type)