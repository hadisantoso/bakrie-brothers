@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.Year extends Entities.Model
    defaults:
      year: "20xx"
    
  class Entities.YearsCollection extends Entities.Collection
    model: Entities.Year
  
  API =
    getYears: (start, end, includeAll) ->
      array = []
      return if start > end
      while(start < end)
        array.push { year: (start + "")}
        start = start + 1
      
      if includeAll
        array.push { year: App.Languages.translate("general.view_all")}

      array.reverse()

      yearCollection = new Entities.YearsCollection array
      yearCollection
        
  App.reqres.setHandler "year:get", (start, end, includeAll) ->
    API.getYears(start, end, includeAll)