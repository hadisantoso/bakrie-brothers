@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.EndUserPost extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.end_user_post_index_path()
  

  class Entities.EndUserPostCollection extends App.Entities.Collection
    model: Entities.EndUserPost
    
    url: -> "https://bakrie-brothers.herokuapp.com" + Routes.end_user_post_index_path()
  
  API =
    getPost: (type) ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          type: type
        reset: true
      post
    
    getPostMember: (id) ->
      member = new Entities.EndUserPost
        id: id
      member.fetch(
        url: "https://bakrie-brothers.herokuapp.com/end_user/post/" + id + ".json"
        data:
          foo: ""
      )
      member

    #queries
    getHighlightPost: ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          query_type: "highlight"
        reset: true
      post

    getLatestPost: (type) ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          type: type
          query_type: "latest"
        reset: true
      post

    getLatestNPost: (type, n) ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          type: type
          query_type: "latestN"
          n: n
        reset: true
      post

    getPosts: (type, filter, filter_type, page, summary) ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          type: type
          filter: filter
          filter_type: filter_type
          page: page
          summary: summary
        reset: true
      post

    getMorePosts: (post, type, page, filter, filter_type, summary) ->
      post.fetch
        data:
          type: type
          filter: filter
          filter_type: filter_type
          page: page
          summary: summary
        remove: false
      post

    getPagePost: (title) ->
      post = new Entities.EndUserPostCollection
      post.fetch
        data:
          query_type: "pages"
          type: "PagePost"
          language: App.language
          title: title
        reset: true
      post

  
  App.reqres.setHandler "end_user:post:entity", (id) ->
    API.getPostMember id
  
  App.reqres.setHandler "end_user:post:highlight", ->
    API.getHighlightPost()

  App.reqres.setHandler "end_user:post:latest", (type) ->
    API.getLatestPost(type)

  App.reqres.setHandler "end_user:post:latestN", (type, n) ->
    API.getLatestNPost(type, n)

  App.reqres.setHandler "end_user:post:get", (type, filter, filter_type, page, summary) ->
    API.getPosts(type, filter, filter_type, page, summary)

  App.reqres.setHandler "end_user:post:get:more", (post, type, page, filter, filter_type, summary) ->
    API.getMorePosts(post, type, page, filter, filter_type, summary)

  App.reqres.setHandler "end_user:post:page:get", (title) ->
    API.getPagePost(title)