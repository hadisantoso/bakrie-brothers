@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.User extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_user_index_path()
  
    uploadDocument: (file, cb) ->
      @uploadFile(file, "/admin/user/#{@id}/upload_document", cb, ->)

    uploadImage: (file, cb) ->
      @uploadFile(file, "/admin/user/#{@id}/upload_image", cb, ->)

  class Entities.UserCollection extends App.Entities.Collection
    model: Entities.User
    
    url: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_user_index_path()
  
  API =
    getUser: ->
      user = new Entities.UserCollection
      user.fetch
        data:
          token: App.token
        reset: true
      user
    
    getUserMember: (id) ->
      member = new Entities.User
        id: id
      member.fetch(
        data:
          token: App.token
      )
      member
    
    newUserMember: ->
      new Entities.User
  
  App.reqres.setHandler "user:entities", ->
    API.getUser()
  
  App.reqres.setHandler "user:entity", (id) ->
    API.getUserMember id
  
  App.reqres.setHandler "new:user:entity", ->
    API.newUserMember()