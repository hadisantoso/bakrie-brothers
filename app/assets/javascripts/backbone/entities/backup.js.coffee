@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.Backup extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_backup_index_path()
  
  class Entities.BackupCollection extends App.Entities.Collection
    model: Entities.Backup
    
    url: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_backup_index_path()
  
  API =
    getBackups: ->
      backups = new Entities.BackupCollection
      backups.fetch
        data:
          token: App.token
        reset: true
      backups
    
  App.reqres.setHandler "admin:backups:get", (filter, filter_type) ->
    API.getBackups()