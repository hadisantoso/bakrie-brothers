@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.AuditRecord extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_auditor_index_path()

  
  class Entities.AuditRecordCollection extends App.Entities.Collection
    model: Entities.AuditRecord
    
    url: -> "https://bakrie-brothers.herokuapp.com" + Routes.admin_auditor_index_path()
  
  API =
    getAuditRecords: (filter, filter_type) ->
      audit_records = new Entities.AuditRecordCollection
      audit_records.fetch
        data:
          token: App.token
          filter: filter
          filter_type: filter_type
        reset: true
      audit_records

    getMoreAuditRecords: (audit_records, page, filter, filter_type) ->
      audit_records.fetch
        data:
          token: App.token
          filter: filter
          filter_type: filter_type
          page: page
        remove: false
      audit_records

  App.reqres.setHandler "admin:audit_records:get", (filter, filter_type) ->
    API.getAuditRecords(filter, filter_type)

  App.reqres.setHandler "admin:audit_records:get:more", (audit_records, page, filter, filter_type) ->
    API.getMoreAuditRecords(audit_records, page, filter, filter_type)