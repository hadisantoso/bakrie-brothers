@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
	
	class Entities.Model extends Backbone.Model
		
		destroy: (options = {}) ->
			_.defaults options,
				wait: true
			
			@set _destroy: true
			super options
		
		isDestroyed: ->
			@get "_destroy"
		
		save: (data, options = {}) ->
			isNew = @isNew()
			
			_.defaults options,
				wait: true
				success: 	_.bind(@saveSuccess, @, isNew, options.collection)
				error:		_.bind(@saveError, @)
		
			data["token"] = App.token
			@unset "_errors"
			super data, options
		
		saveSuccess: (isNew, collection) =>
			if isNew ## model is being created
				collection.add @ if collection
				collection.trigger "model:created", @ if collection
				@trigger "created", @
			else ## model is being updated
				collection ?= @collection ## if model has collection property defined, use that if no collection option exists
				collection.trigger "model:updated", @ if collection
				@trigger "updated", @
		
		saveError: (model, xhr, options) =>
			## set errors directly on the model unless status returned was 500 or 404
			@set _errors: $.parseJSON(xhr.responseText)?.errors unless xhr.status is 500 or xhr.status is 404

		uploadFile: (file, url, successTriggerEvent, cb) ->			
			myXhr = @setupProgressTracking
			data = new FormData()
			data.append("file", file)
			data.append("token", App.token)
			$.ajax
				url: url
				data: data
				cache: false
				contentType: false
				processData: false
				type: 'POST'
				xhr: myXhr
				timeout: 300000
				success: (data) =>
					toastr.success('Uploaded')
					@fetch(
						data:
							token: App.token
						success: =>
							@trigger successTriggerEvent, @
							cb()
					)  
				error: (data) ->
					toastr.error('Error uploading')
					cb()

		setupProgressTracking: =>
			myXhr = $.ajaxSettings.xhr()
			if myXhr.upload
				myXhr.upload.onprogress = (e) ->
					console.log e
					if(e.lengthComputable)
						console.log "#{e.loaded}/#{e.total}"
			return myXhr