@BBApp.module "Entities", (Entities, App, Backbone, Marionette, $, _) ->
  
  class Entities.Auth extends App.Entities.Model
    urlRoot: -> "https://bakrie-brothers.herokuapp.com" + Routes.authenticate_admin_auth_index_path()

  API =    
    newAuth: ->
      new Entities.Auth
  
  App.reqres.setHandler "new:auth:entity", ->
    API.newAuth()