// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jade/runtime
//= require lib/jquery
//= require lib/underscore
//= require lib/underscore.string
//= require lib/backbone
//= require lib/marionette
//= require lib/syphon
//= require lib/toastr
//= require lib/bootstrap
//= require lib/html5shiv
//= require lib/jquery.fancybox.pack
//= require lib/helpers/jquery.fancybox-buttons
//= require lib/helpers/jquery.fancybox-media
//= require lib/helpers/jquery.fancybox-thumbs
//= require lib/jquery.isotope.min
//= require lib/jquery.navgoco
//= require lib/jquery.prettyPhoto
//= require lib/respond.min
//= require lib/wow.min
//= require lib/js.cookie
//= require lib/polyglot
//= require js-routes
//= require_tree ./backbone/config
//= require backbone/app
//= require_tree ./backbone/i18n
//= require_tree ./backbone/controllers
//= require_tree ./backbone/views
//= require_tree ./backbone/entities
//= require_tree ./backbone/components
//= require_tree ./backbone/apps
//= require lib/main