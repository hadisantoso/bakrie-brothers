class Admin::AuditorController < ApplicationController
  respond_to :json
  
  before_filter :set_paging_params
  before_filter :require_super_admin

  def set_paging_params
    @per_page = params[:per_page] ? params[:per_page].to_i : 15
    @page_num = params[:page] ? params[:page].to_i : 1
    @to_skip = ( @page_num - 1 ) * (@per_page)
    @max_index = @per_page * @page_num
  end

  def require_super_admin
    puts "require super admin"
    token = params[:token]
    user = User.where(:token => token).first
    if user.clearance != User::ROLE_SUPER_ADMIN
      raise "invalid user"
    end
  end

  def index
    # sleep 5
    @audit_records = AuditRecord.all.desc(:created_at).skip(@to_skip).limit(@per_page)
    render "audit_record/index.json.rabl"
  end

end