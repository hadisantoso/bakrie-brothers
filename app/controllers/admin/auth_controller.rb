class Admin::AuthController < ApplicationController
  respond_to :json
  
  def authenticate
    # sleep 5
    nickname = params[:nickname]
    password = params[:password]

    @user = User.authenticate(nickname, password)

    render "user/login_response.json.rabl"
  end
end