class Admin::CrewController < ApplicationController
  respond_to :json
  
  def index
    # sleep 5
    @crew = Crew.all
    render "crew/index"
  end
  
  def show
    # sleep 5
    @member = Crew.find params[:id]
    render "crew/show"
  end
  
  def update
    @member = Crew.find params[:id]
    if @member.update_attributes crew_params(params)
      render "crew/show"
    else
      respond_with @member
    end
  end
  
  def create
    @member = Crew.new
    if @member.update_attributes crew_params(params)
      render "crew/show"
    else
      render "crew/show"
    end
  end
  
  def destroy
    sleep 2
    member = Crew.find params[:id]
    member.destroy()
    render json: {}
  end

  def upload_image
    @crew = Crew.find(params[:crew_id])
    @crew.image = params["file"] if params["file"]

    if @crew.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@crew.errors.full_messages}
    end
  end

  def upload_document
    @crew = Crew.find(params[:crew_id])
    @crew.document = params["file"] if params["file"]

    if @crew.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@crew.errors.full_messages}
    end
  end

  private

  def crew_params(params)
    puts params
    params.require(:crew).permit(:age, :avatar, :name, :origin, :quote, :species, :title)
  end
end