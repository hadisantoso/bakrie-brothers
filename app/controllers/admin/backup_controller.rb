class Admin::BackupController < ApplicationController
  require 'ostruct'    
  respond_to :json
  before_filter :require_super_admin

  def require_super_admin
    puts "require super admin"
    token = params[:token]
    user = User.where(:token => token).first
    if user.clearance != User::ROLE_SUPER_ADMIN
      raise "invalid user"
    end
  end

  def index
    uri = URI.parse("https://api.compose.io/accounts/motimoto/backups")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    req = Net::HTTP::Get.new(uri.path)
    req['Content-Type'] ='application/json'
    req['Accept-Version'] ='2014-06'
    req['Authorization'] = "Bearer #{ENV['COMPOSE_IO_TOKEN']}"
    res = https.request(req)
    puts "Response #{res.code} #{res.message}: #{res.body}"

    parsedJSON = JSON.parse(res.body)
    
    @backups = []
    parsedJSON.each do |backup|
      backup = OpenStruct.new(backup)
      res = []
      backup.links.each do |link|
        l = OpenStruct.new(link)
        res << l if l.rel == "temp-download" 
      end
      backup.links = res
      @backups << backup
    end

    render "/backup/index.json.rabl"

  end

end




# curl -i -X GET 'https://api.compose.io/accounts/motimoto/backups' \
# -H 'Content-Type: application/json' \
# -H 'Accept-Version: 2014-06' \
# -H 'Authorization: Bearer d770142fff0deb86f2dd1ce8fbcab4a3da39b9617718efa146465034b17b6e1bb97c7163feb8f6e369a2fcba36e695ee1a26a522bbe85171ad707ef091317e62'


# curl -i -X GET 'https://api.compose.io/accounts/motimoto/backups/55c181fb27b4b1e5000010f9/download' \
# -H 'Content-Type: application/json' \
# -H 'Accept-Version: 2014-06' \
# -H 'Authorization: Bearer d770142fff0deb86f2dd1ce8fbcab4a3da39b9617718efa146465034b17b6e1bb97c7163feb8f6e369a2fcba36e695ee1a26a522bbe85171ad707ef091317e62'