class Admin::PostController < ApplicationController
  respond_to :json
  
  before_filter :set_paging_params
  before_filter :require_admin

  def require_admin
    token = params[:token]
    @current_user = User.where(:token => token).first
    if @current_user.clearance != User::ROLE_ADMIN && @current_user.clearance != User::ROLE_SUPER_ADMIN && @current_user.clearance != User::ROLE_SUPER_DUPER_ADMIN 
      raise "invalid user"
    end
  end

  def set_paging_params
    @per_page = params[:per_page] ? params[:per_page].to_i : 10
    @page_num = params[:page] ? params[:page].to_i : 1
    @to_skip = ( @page_num - 1 ) * (@per_page)
    @max_index = @per_page * @page_num
  end

  def index
    query_type = params[:query_type]

    if params[:type]
      @post = Post.get_post_criteria(@current_user, params[:type].camelize)
      @post = @post.where(:status.ne => Post::STATUS_DELETED).desc(:post_date)

      if params[:type].camelize.constantize == PagePost
        @post = @post.where(:human_readable_key.in => @current_user.page_access)
      end

      if params[:page]
        @post = @post.skip(@to_skip).limit(@per_page)
      end    
    else
      @post = []
    end
    render "post/index.json.rabl"
  end
  
  def show
    @post = Post.find params[:id]
    render "post/show.json.rabl"
  end
  
  def update
    @post = Post.find params[:id]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if params[:remove_image]    
      image_to_remove = params[:remove_image]
      if image_to_remove == "image"
        @post.remove_image = true
      elsif image_to_remove == "image2"
        @post.remove_image2 = true
      elsif image_to_remove == "image3"
        @post.remove_image3 = true
      elsif image_to_remove == "image4"
        @post.remove_image4 = true
      end

      @post.on(:after_update) do |post| 
        Wispers::PostWisper.update_post_successful(post, @current_user)
      end
      res = @post.save

      puts "res: #{res}"

      if res
        render "post/show.json.rabl"
      else
        respond_with @post
      end
    elsif @post.update_attributes post_params(params)
      render "post/show.json.rabl"
    else
      respond_with @post
    end
  end
  
  def create
    user = User.where(:token => params[:token]).first
    @post = Post.create_post(params[:title], params[:title], params[:markdown], params[:markdown], params[:summary_markdown], params[:summary_markdown], user, params[:_type], params[:post_date], params[:author], params[:highlight])
    render "post/show.json.rabl"  
  end

  def upload_image
    @post = Post.find(params[:post_id])
    @post.image = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  def upload_image2
    @post = Post.find(params[:post_id])
    @post.image2 = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  def upload_image3
    @post = Post.find(params[:post_id])
    @post.image3 = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  def upload_image4
    @post = Post.find(params[:post_id])
    @post.image4 = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  def upload_header_image
    @post = Post.find(params[:post_id])
    @post.header_image = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end  

  def upload_document
    @post = Post.find(params[:post_id])
    @post.document = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  def upload_document2
    @post = Post.find(params[:post_id])
    @post.document2 = params["file"] if params["file"]
    @post.on(:after_update) do |post| 
      Wispers::PostWisper.update_post_successful(post, @current_user)
    end

    if @post.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@post.errors.full_messages}
    end
  end

  private

  def post_params(params)
    params.require(:post).permit(:author, :title, :title2, :markdown, :markdown2, :summary_markdown, :summary_markdown2, :post_date, :highlight, :status, :language, :human_readable_key)
  end
end