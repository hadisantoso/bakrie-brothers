class Admin::UserController < ApplicationController
  respond_to :json

  before_filter :require_super_duper_admin

  def require_super_duper_admin
    token = params[:token]
    @current_user = User.where(:token => token).first
    if @current_user.clearance != User::ROLE_SUPER_DUPER_ADMIN
      raise "invalid user"
    end
  end
  
  def index
    # sleep 5
    @user = User.all
    render "user/index.json.rabl"
  end
  
  def show
    # sleep 5
    @user = User.find params[:id]
    render "user/show.json.rabl"
  end
  
  def create
    @user = User.new

    @user.on(:after_create) do |user| 
      Wispers::UserWisper.create_user_successful(user, @current_user)
    end

    @user.nickname = params[:nickname]
    @user.save!
    render "user/show.json.rabl"
  end

  def update
    @user = User.find params[:id]
    @user.on(:after_update) do |user| 
      Wispers::UserWisper.update_user_successful(user, @current_user)
    end

    @user.nickname = params[:nickname]
    @user.password = params[:password]
    @user.email = params[:email]
    @user.name = params[:name]
    @user.department = params[:department]
    @user.post_access = params[:post_access].split(",")
    @user.page_access = params[:page_access].split(",")
    @user.setup_password()

    if @user.save
      render "user/show.json.rabl"
    else
      respond_with @user
    end
  end

  def upload_image
    @user = User.find(params[:user_id])
    @user.image = params["file"] if params["file"]
    @user.on(:after_update) do |user| 
      Wispers::UserWisper.update_user_successful(user)
    end

    if @user.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@user.errors.full_messages}
    end
  end

  def upload_document
    @user = User.find(params[:user_id])
    @user.document = params["file"] if params["file"]
    @user.on(:after_update) do |user| 
      Wispers::UserWisper.update_user_successful(user)
    end

    if @user.save
      render json: {:success => true}
    else
      render :json=>{:success => false, :errors=>@user.errors.full_messages}
    end
  end

  private

  def user_params(params)
    params.require(:user).permit(:nickname, :email, :name, :department, :post_access, :page_access)
  end
end