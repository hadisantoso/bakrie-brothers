class EndUser::PostController < ApplicationController
  respond_to :json
  
  before_filter :set_paging_params


  def set_paging_params
    @per_page = params[:per_page] ? params[:per_page].to_i : 10
    @page_num = params[:page] ? params[:page].to_i : 1
    @to_skip = ( @page_num - 1 ) * (@per_page)
    @max_index = @per_page * @page_num
  end


  def index
    query_type = params[:query_type]

    if query_type
      if query_type == "highlight"
        @post = Post.where(:status => Post::STATUS_PUBLISHED, :highlight => Post::HIGHLIGHT_HOMEPAGE).first
      elsif query_type == "latest"
        @post = Post.where(:status => Post::STATUS_PUBLISHED, :_type => params[:type]).desc(:post_date).first
      elsif query_type == "latestN"
        n = params[:n]
        @post = Post.where(:status => Post::STATUS_PUBLISHED, :_type => params[:type]).desc(:post_date).limit(n)
      elsif query_type == "pages"
        @post = Post.where(:status => Post::STATUS_PUBLISHED, :_type => params[:type], :human_readable_key => params[:title]).first
      end
    elsif params[:type]
      _type = params[:type].camelize
      @post = Post.where(:_type => _type)
      
      if params[:filter_type] == "post_date"
        filter = params[:filter].downcase
        if filter != "view all" && filter != "lihat lainnya" && filter != "last5" && !filter.empty?
          year = filter.to_i
          start = Time.now.change(:year => year).beginning_of_year
          finish = Time.now.change(:year => year).end_of_year
          @post = @post.where(:post_date.gte => start, :post_date.lte => finish)
        elsif filter == "last5"
          start = Time.now.change(:year => (Time.now.year() - 4)).beginning_of_year
          finish = Time.now.end_of_year
          @post = @post.where(:post_date.gte => start, :post_date.lte => finish)
        end
      end

      @post = @post.where(:status => Post::STATUS_PUBLISHED).desc(:post_date)

      if params[:page]
        @post = @post.skip(@to_skip).limit(@per_page)
      end    

      count = 0
      @post = @post.map do |p|
        p[:order] = count
        count += 1
        p
      end

    else
      @post = []
    end

    if params[:summary]
      render "post/read_only_index_summary.json.rabl"
    else
      render "post/read_only_index.json.rabl"
    end
  end
  
  def show
    # sleep 5
    @post = Post.find params[:id]
    render "post/read_only_show"
  end
end