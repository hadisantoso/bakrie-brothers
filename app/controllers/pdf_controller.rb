class PdfController < ApplicationController
  respond_to :json
  
  layout "pdf"

  def corporate_history
    @language = "id"
    @intro_post = PagePost.where(:human_readable_key => "corporate history").first
    @posts = CorporateHistoryPost.where(:status => Post::STATUS_PUBLISHED).desc(:post_date)
    count = 0
    @posts = @posts.map do |p|
      p[:order] = count
      count += 1
      p
    end
  end

  def page
    @language = params[:language]
    @post = PagePost.where(:human_readable_key => params[:page]).first
    @title = ""
    @customMarkdown = ""

    @images = []
    @images << @post.image.url if @post.image_url
    @images << @post.image2.url if @post.image2_url
    @images << @post.image3.url if @post.image3_url
    @images << @post.image4.url if @post.image4_url

    if @language == Post::LANGUAGE_INDONESIAN      
      @title = @post.title
      @customMarkdown = @post.custom_markdown_html
    elsif @language == Post::LANGUAGE_ENGLISH
      @title = @post.title2
      @customMarkdown = @post.custom_markdown_html2
    end
  end
end