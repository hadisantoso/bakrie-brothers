class TestCode

  def self.create_random_posts    
    user = User.last
    types = ["csr","event","news","pr","profile","report"]
    (1...10).each do |i|
      post = TestCode.create_random_post_type
      post.author = user.nickname
      post.title = "Random title #{rand(1000)}"
      post.markdown = "Test body #{rand(1000)}"      
      post.user = user
      post.save!
    end
  end

  def self.create_random_post_type
    types = [CsrPost.new,EventPost.new,NewsPost.new,PrPost.new,ProfilePost.new,ReportPost.new]
    return types[rand(types.count)]
  end
end