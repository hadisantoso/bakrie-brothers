class PasswordManager
  def setup_password(user)
    user = User.find_all(user_id)
    puts "setup password"
    puts user.password.present?
    if user.password.present?
      user.password_salt = BCrypt::Engine.generate_salt
      user.password_hash = BCrypt::Engine.hash_secret(user.password, user.password_salt)
      user.save!
    end
  end
end