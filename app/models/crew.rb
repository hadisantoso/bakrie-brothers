class Crew
  include Mongoid::Document
  include Mongoid::Timestamps

  field :age, type: Integer
  field :avatar
  field :name
  field :origin
  field :quote
  field :species
  field :title

  mount_uploader :image, ImageUploader, mount_on: :image_filename  
  field :image_width, type: Integer
  field :image_height, type: Integer

  mount_uploader :document, DocumentUploader, mount_on: :document_filename

  validates_presence_of :name
  validates_presence_of :title, :origin, :age, :avatar, :species, on: :update
end
