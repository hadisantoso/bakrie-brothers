class Wispers::UserWisper

  def self.create_user_successful(user, current_user)
    user.generate_token()
    user.save!(:validate => false)
    AuditRecord.create_audit_record(user.class.name, "create", user, current_user)
  end

  def self.update_user_successful(user, current_user)
    AuditRecord.create_audit_record(user.class.name, "update", user, current_user)
  end

end