class Wispers::PostWisper

  def self.create_post_successful(post, user)
    if post.highlight == Post::HIGHLIGHT_HOMEPAGE
      Post.where(:id.ne => post.id.to_s).update_all(:highlight => 0)
    end
    AuditRecord.create_audit_record(post.class.name, "create", post, user)
  end

  def self.update_post_successful(post, user)
    if post.highlight == Post::HIGHLIGHT_HOMEPAGE
      Post.where(:id.ne => post.id.to_s).update_all(:highlight => 0)
    end
    AuditRecord.create_audit_record(post.class.name, "update", post, user)
  end

end