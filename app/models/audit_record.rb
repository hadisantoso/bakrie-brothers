class AuditRecord
  include Mongoid::Document
  include Mongoid::Timestamps

  field :document_class
  field :document_event
  field :document_changes, type: Hash
  field :evoker_nickname

  def self.create_audit_record(document_class, document_event, document, user)
    if document.changed?
      audit_record = AuditRecord.new
      audit_record.document_class = document_class
      audit_record.document_event = document_event
      audit_record.document_changes = document.changes
      audit_record.evoker_nickname = user.nickname
      audit_record.save!
    end
  end
end