class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Modules::Models::User::Security
  include Modules::Models::User::Query

  include Wisper.model

  field :nickname
  field :name
  field :screen_name
  field :email

  field :token

  DEPARTMENT_NONE="None"
  field :department, default: DEPARTMENT_NONE

  ROLE_USER="user"
  ROLE_ADMIN="admin"
  ROLE_SUPER_ADMIN="super_admin"
  ROLE_SUPER_DUPER_ADMIN="super_duper_admin"
  field :clearance, default: ROLE_ADMIN
  field :post_access, type: Array, default: []
  field :page_access, type: Array, default: []

  attr_accessor :password
  field :password_salt
  field :password_hash

  field :password_reset_token
  field :password_reset_sent_at, type: Time

  mount_uploader :image, ImageUploader, mount_on: :image_filename  
  field :image_width, type: Integer
  field :image_height, type: Integer

  validates_presence_of  :nickname
  validates_presence_of  :email, :name, :on => :update
  validates :email, uniqueness: true, :on => :update
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :update

  validates_uniqueness_of :nickname
  validate :check_nickname_uniqueness

  has_many :posts

  def self.register_user(nickname, name, screen_name, email, department, clearance, password, post_access, page_access)
    user = User.new
    user.nickname = nickname
    user.name = name
    user.email = email
    user.screen_name = screen_name
    user.department = department
    user.clearance = clearance
    user.password = password
    user.post_access = post_access
    user.page_access = page_access

    user.on(:after_create) do |user| 
      Wispers::UserWisper.create_user_successful(user)
    end

    user.save!
  end

  def update_password(password)
    self.password = password
    self.setup_password
    self.save!
  end

  def check_nickname_uniqueness
    already_taken = User.find_all(self.nickname)
    errors.add(:nickname, "already taken") if self.new_record? && already_taken
  end
end
