class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  include Wisper.model

  field :title
  field :title2

  field :human_readable_key

  field :markdown
  field :markdown2

  field :summary_markdown
  field :summary_markdown2

  field :post_date, type: Time
  field :author

  LANGUAGE_INDONESIAN="id"
  LANGUAGE_ENGLISH="en"
  field :language, default: LANGUAGE_INDONESIAN

  HIGHLIGHT_NONE=0
  HIGHLIGHT_HOMEPAGE=1
  field :highlight, type: Integer, default: HIGHLIGHT_NONE

  mount_uploader :image, ImageUploader, mount_on: :image_filename
  mount_uploader :image2, ImageUploader, mount_on: :image2_filename
  mount_uploader :image3, ImageUploader, mount_on: :image3_filename
  mount_uploader :image4, ImageUploader, mount_on: :image4_filename

  mount_uploader :header_image, ImageUploader, mount_on: :header_image_filename

  mount_uploader :document, DocumentUploader, mount_on: :document_filename
  mount_uploader :document2, DocumentUploader, mount_on: :document_filename2

  validates_presence_of :title, :user, :post_date

  belongs_to :user

  STATUS_DELETED="deleted"
  STATUS_UNPUBLISHED="unpublished"
  STATUS_PUBLISHED="published"
  field :status, default: STATUS_UNPUBLISHED

  def custom_markdown_html
    return "" if self.markdown.nil?
    m = self.markdown
    res = m.scan(/image=.*/) 
    res.each do |image_str|
      image_name = image_str.split("=").last()
      imagePost = ImagePost.or({:title => image_name}, {:title2 => image_name}).first

      next if imagePost.nil?
      m = m.gsub(image_str, "<a href='#{imagePost.image.url}' target='_blank'><image src='#{imagePost.image.url}'/></a>")
    end

    doc = Maruku.new(m)
    doc.to_html.gsub("\n","")
  end

  def custom_markdown_html2
    return "" if self.markdown.nil?
    m = self.markdown2
    res = m.scan(/image=.*/) 
    res.each do |image_str|
      image_name = image_str.split("=").last()
      imagePost = ImagePost.or({:title => image_name}, {:title2 => image_name}).first

      next if imagePost.nil?
      m = m.gsub(image_str, "<a href='#{imagePost.image.url}' target='_blank'><image src='#{imagePost.image.url}'/></a>")
    end

    doc = Maruku.new(m)
    doc.to_html.gsub("\n","")
  end


  def markdown_html
    doc = Maruku.new(markdown)
    doc.to_html.gsub("\n","")
  end

  def markdown_html2
    doc = Maruku.new(markdown2)
    doc.to_html.gsub("\n","")
  end


  def summary_markdown_html
    doc = Maruku.new(summary_markdown)
    doc.to_html.gsub("\n","")
  end

  def summary_markdown_html2
    doc = Maruku.new(summary_markdown2)
    doc.to_html.gsub("\n","")
  end

  def self.create_post(title, title2, markdown, markdown2, summary_markdown, summary_markdown2, user, _type, post_date, author, highlight)
    post = Post.get_post_instance(_type)
    post.title = title
    post.title2 = title2
    post.markdown = markdown
    post.markdown2 = markdown2
    post.summary_markdown = summary_markdown
    post.summary_markdown2 = summary_markdown2
    post.user = user
    post.post_date = post_date.nil? ? Time.now : post_date
    post.author = author
    post.highlight = highlight

    post.on(:after_create) do |post| 
      Wispers::PostWisper.create_post_successful(post, user)
    end
    post.save!

    return post
  end

  def self.get_post_instance(_type)
    _type = _type.camelize
    if _type == "DirectorPost"
      return DirectorPost.new
    elsif _type == "CommissionerPost"
      return CommissionerPost.new
    elsif _type == "CompanyPresentationPost"
      return CompanyPresentationPost.new
    elsif _type == "FinancialReportPost"
      return FinancialReportPost.new
    elsif _type == "AnnualReportPost"
      return AnnualReportPost.new
    elsif _type == "CsrPost"
      return CsrPost.new
    elsif _type == "HsePost"
      return HsePost.new
    elsif _type == "EventPost"
      return EventPost.new
    elsif _type == "NewsPost"
      return NewsPost.new
    elsif _type == "PrPost"
      return PrPost.new
    elsif _type == "RupsPost"
      return RupsPost.new
    elsif _type == "PublicExposePost"
      return PublicExposePost.new
    elsif _type == "CorrespondencePost"
      return CorrespondencePost.new
    elsif _type == "PagePost"
      return PagePost.new
    elsif _type == "CorporateGovernancePost"
      return CorporateGovernancePost.new
    elsif _type == "CorporateHistoryPost"
      return CorporateHistoryPost.new
    elsif _type == "ImagePost"
      return ImagePost.new
    elsif _type == "HomeImagePost"
      return HomeImagePost.new
    elsif _type == "CorporateSecretaryPost"
      return CorporateSecretaryPost.new
    elsif _type == "AnnouncementDocumentPost"
      return AnnouncementDocumentPost.new
    elsif _type == "CorporateGovernanceDocumentPost"
      return CorporateGovernanceDocumentPost.new
    else
      raise "Invalid post type"
    end
  end

  def self.get_post_criteria(user, type)
    raise "Post - Access denied" if !user.post_access.include?(type)
    return Post.where(:_type => type)
  end
end