attributes :document_class, :document_event, :document_changes, :evoker_nickname

node(:formatted_created_at) {|h| h.created_at.in_time_zone("Jakarta").strftime("%d %b %Y, %R")}