attributes :nickname, :name, :screen_name, :email, :department, :post_access, :page_access

node (:id) { |h| h.id.to_s}
node(:image) do |h|
  if h.image_url
    {
    :standard_resolution =>  h.image.standard_resolution.url,
    :low_resolution => h.image.low_resolution.url,
    :thumbnail_resolution =>  h.image.thumbnail.url
    }
  else
    {
    :standard_resolution =>  "http://placehold.it/612x612",
    :low_resolution => "http://placehold.it/150x150",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end