object @user

attributes :nickname, :department, :post_access, :page_access

if @user.nil?
  node(:token) { |h| nil}
else
  node(:token) { |h| h.token}
end