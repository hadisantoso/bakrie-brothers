attributes :type, :status, :filename, :size

node(:created_at) {|h| DateTime.parse(h.created_at).strftime("%d %h %y")}
child :links => :links do
  extends "/backup/link"
end