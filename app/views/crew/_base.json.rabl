attributes :id, :age, :name, :avatar, :title, :species, :origin, :quote

node (:id) { |h| h.id.to_s}
node(:image) do |h|
  if h.image_url
    {
    :standard_resolution =>  h.image.standard_resolution.url,
    :low_resolution => h.image.low_resolution.url,
    :thumbnail_resolution =>  h.image.thumbnail.url
    }
  else
    {
    :standard_resolution =>  "http://placehold.it/612x612",
    :low_resolution => "http://placehold.it/150x150",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end
node(:document) do |h|
  if h.document_url
    {
    :document_url =>  h.document_url
    }
  else
    {
    :document_url =>  "http://placehold.it/612x612"
    }    
  end
end