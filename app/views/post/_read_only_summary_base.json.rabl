attributes :title, :title2, :summary_markdown_html, :summary_markdown_html2, :author

node (:id) { |h| h.id.to_s}
node(:formatted_post_date) {|h| h.post_date.nil? ? "" : h.post_date.strftime("%d %b %Y")}

node(:image) do |h|
  if h.image_url
    {
    :present => true,
    :original_resolution =>  h.image.url,
    :standard_resolution =>  h.image.standard_resolution.url,
    :low_resolution => h.image.low_resolution.url,
    :thumbnail_resolution =>  h.image.thumbnail.url
    }
  else
    {
    :present => false,
    :original_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :standard_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :low_resolution => "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end

node(:image2) do |h|
  if h.image2_url
    {
    :present => true,
    :original_resolution =>  h.image2.url,
    :standard_resolution =>  h.image2.standard_resolution.url,
    :low_resolution => h.image2.low_resolution.url,
    :thumbnail_resolution =>  h.image2.thumbnail.url
    }
  else
    {
    :present => false,
    :original_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :standard_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :low_resolution => "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end

node(:image3) do |h|
  if h.image3_url
    {
    :present => true,
    :original_resolution =>  h.image3.url,
    :standard_resolution =>  h.image3.standard_resolution.url,
    :low_resolution => h.image3.low_resolution.url,
    :thumbnail_resolution =>  h.image3.thumbnail.url
    }
  else
    {
    :present => false,
    :original_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :standard_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :low_resolution => "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end

node(:image4) do |h|
  if h.image4_url
    {
    :present => true,
    :original_resolution =>  h.image4.url,
    :standard_resolution =>  h.image4.standard_resolution.url,
    :low_resolution => h.image4.low_resolution.url,
    :thumbnail_resolution =>  h.image4.thumbnail.url
    }
  else
    {
    :present => false,
    :original_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :standard_resolution =>  "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :low_resolution => "https://s3-ap-southeast-1.amazonaws.com/bakriebrothersstatic/defaults/default-image.jpg",
    :thumbnail_resolution =>  "http://placehold.it/100x100"
    }    
  end
end

node(:images) do |h|
  images = []
  images << h.image.url if h.image_url
  images << h.image2.url if h.image2_url
  images << h.image3.url if h.image3_url
  images << h.image4.url if h.image4_url
  images
end


node(:order) { |h| h[:order] }